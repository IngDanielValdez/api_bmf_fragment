import { IsString, IsArray, IsNumber, Min, IsBoolean, IsEnum, IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

export class CreateDealDTO {
    @IsNotEmpty()
    @IsString()
    companyID: string;

    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    quantity: number;

    @IsOptional()
    @IsString()
    @MaxLength(100)
    source: string;

    @IsNotEmpty()
    @IsArray()
    @IsEnum(['USA', 'CANADA', 'PR', 'UK'], { each: true })
    territories: string[];
}
