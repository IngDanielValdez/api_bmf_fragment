import { IsString, IsNotEmpty, IsEnum, MaxLength } from 'class-validator';

export class ChangeStatusDTO {
    @IsNotEmpty()
    @IsEnum(['sended', 'notsent', 'rejected', 'approvednotfunded', 'onhold'])
    status: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(1000)
    message: string;
}
