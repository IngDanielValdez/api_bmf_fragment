import { IsString, IsNumber, Min, IsEnum, IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateNotifyDTO {
    @IsNotEmpty()
    @IsString()
    notifyID: string;

    @IsNotEmpty()
    @IsEnum(['rejected', 'accepted', 'onhold', 'replied'])
    status: string[];

    @IsOptional()
    @IsNumber()
    @Min(0)
    approvedAmount: number;
}
