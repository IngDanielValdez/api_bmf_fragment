import { IsString, IsArray, IsNotEmpty, IsOptional, MaxLength } from 'class-validator';

export class AddBankDTO {
    @IsNotEmpty()
    @IsArray()
    @IsString({ each: true })
    banksIDs: string[];

    @IsOptional()
    @IsString()
    @MaxLength(1000)
    message: string;
}
