import { IsString, IsNotEmpty, IsNumber, Min } from 'class-validator';

export class CloseDealDTO {
    @IsNotEmpty()
    @IsString()
    bankID: string;

    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    closingAmount: number;
}
