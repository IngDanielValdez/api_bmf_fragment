import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Deal } from './interfaces/deal.interface';
import { CreateDealDTO } from './DTO/create-deal.dto';

@Injectable()
export class DealsService {
    constructor(@InjectModel('Deal') private DealModel: Model<Deal>) {}

    async getDeals(query?, infinity = false): Promise<Deal[]> {
        const { limit, skip, options } = query;
        let limitPage = Math.abs((Number(limit) < 100 ? Number(limit) : 10)) || 10;
        if (infinity) { limitPage = undefined; }
        return await this.DealModel.find(options)
        .limit(limitPage)
        .skip(Math.abs(Number(skip)) || 0)
        .populate('insertBy', 'email name')
        .populate('companyID', 'name code taxid dba state')
        .populate('submittedBy', 'name email role')
        .sort({ lastSubmitted: 'desc' });
    }

    async getDealsForReport(query): Promise<Deal[]> {
        return await this.DealModel.find(query)
        .populate('insertBy', 'email name')
        .populate('companyID', 'name code taxid dba state')
        .populate('submittedBy', 'name email role')
        .populate('banksIDs.bankID', 'name')
        .sort({ lastSubmitted: 'desc' });
    }

    async exportDeals(query?): Promise<Deal[]> {
        const { options } = query;
        return await this.DealModel.find(options)
        .populate('insertBy', 'email name')
        .populate('companyID', 'name code taxid dba state')
        .populate('submittedBy', 'name email role')
        .populate('banksIDs.bankID', 'name')
        .populate('closingHistory.bankID', 'name')
        .sort({ lastSubmitted: 'desc' });
    }

    async getDeal(options: object): Promise<Deal> {
        return await this.DealModel.findOne(options)
        .populate('insertBy', 'email name')
        .populate('companyID', 'name code taxid dba state')
        .populate('submittedBy', 'name email role')
        .populate('banksIDs.bankID', 'name email')
        .populate('lifeHistory.insertBy', 'name')
        .populate('closingHistory.bankID', 'name')
        .populate('closingHistory.userID', 'name');
    }

    async countDeals(query): Promise<number> {
        const { options } = query;
        return await this.DealModel.countDocuments(options);
    }

    async createDeal(deal: Deal | CreateDealDTO): Promise<Deal> {
        const dealDB = await new this.DealModel(deal);
        return await dealDB.save();
    }

    async updateDeal(update, query: {}): Promise<Deal> {
        return await this.DealModel.findOneAndUpdate(query, update, { new: true, runValidators: true, context: 'query' })
        .populate('insertBy', 'email name')
        .populate('companyID', 'name code taxid dba')
        .populate('submittedBy', 'name email role')
        .populate('banksIDs.bankID', 'name')
        .populate('lifeHistory.insertBy', 'name')
        .populate('closingHistory.bankID', 'name')
        .populate('closingHistory.userID', 'name');
    }

    async deleteDeal(ID: string): Promise<Deal> {
        return await this.DealModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
}
