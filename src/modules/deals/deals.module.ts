import { Module } from '@nestjs/common';
import { DealsService } from './deals.service';
import { DealsController } from './deals.controller';
import { DealSchema } from './schemas/deal.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { CompaniesModule } from '../companies/companies.module';
import { PostmanModule } from '../postman/postman.module';
import { BanksModule } from '../banks/banks.module';
import { UsersModule } from '../users/users.module';
import { ReportsModule } from '../reports/reports.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Deal', schema: DealSchema }]), CompaniesModule,
  PostmanModule, BanksModule, UsersModule, ReportsModule],
  providers: [DealsService],
  controllers: [DealsController],
  exports: [DealsService],
})
export class DealsModule {}
