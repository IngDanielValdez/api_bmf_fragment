import { Schema } from 'mongoose';

export const ClosureReportsSchema = new Schema({
    bankID: {
        type: Schema.Types.ObjectId,
        ref: 'Bank',
        required: true,
    },
    closingDate: {
        type: Date,
        default: Date.now,
    },
    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    commission: {
        type: Number,
        required: false,
    },
    psf: {
        type: Number,
        required: false,
    },
    closingAmount: {
        type: Number,
        required: true,
    },
});
