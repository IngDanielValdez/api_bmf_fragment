import { Schema } from 'mongoose';
import { BankNotify } from './bank-notify.schema';
import { ClosureReportsSchema } from './closure-reports.schema';
import { DealHistory } from './life-history.schema';

export const DealSchema = new Schema({
    companyID: {
        type: Schema.Types.ObjectId,
        ref: 'Companies',
        required: true,
    },
    companyCode: {
        type: String,
        required: true,
        unique: true,
    },
    source: {
        type: String,
        maxlength: 100,
    },
    banksIDs: [BankNotify],
    quantity: {
        type: Number,
        min: 0,
    },
    period: {
        type: String,
        trim: true,
    },
    status: {
        type: String,
        enum: {
            values: ['sended', 'notsent', 'rejected', 'completed', 'approvednotfunded', 'onhold', 'pending'],
            message: 'Invalid status',
        },
        default: 'pending',
    },
    closingHistory: [ClosureReportsSchema],
    lifeHistory: [DealHistory],
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    lastSubmitted: {
        type: Date,
    },
    isRenovation: {
        type: Boolean,
        default: false,
    },
    submittedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    territories: [{
        type: String,
        enum: ['USA', 'CANADA', 'PR', 'UK'],
        required: true,
    }],
    lastClosingDate: {
        type: Date,
        required: false,
    },
});
