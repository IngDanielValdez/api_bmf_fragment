import { Schema } from 'mongoose';

export const DealHistory = new Schema({
    status: {
        type: String,
        enum: {
            values: ['sended', 'notsent', 'rejected', 'completed', 'approvednotfunded', 'onhold', 'pending'],
            message: 'Invalid status',
        },
        default: 'notsent',
    },
    statusReason: {
        type: String,
        maxlength: 1200,
        trim: true,
    },
    dateChange: {
        type: Date,
        default: Date.now,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});
