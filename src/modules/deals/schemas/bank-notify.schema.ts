import { Schema } from 'mongoose';

export const BankNotify = new Schema({
    bankID: {
        type: Schema.Types.ObjectId,
        ref: 'Bank',
        required: true,
    },
    notify: {
        type: Boolean,
        default: false,
    },
    status: {
        type: String,
        enum: {
            values: ['rejected', 'accepted', 'onhold', 'replied'],
            message: 'Invalid status',
        },
        default: 'onhold',
    },
    approvedAmount: {
        type: Number,
        min: 0,
        default: 0,
    },
    addedDate: {
        type: Date,
    },
    lastNotify: {
        type: Date,
    },
    updateDate: {
        type: Date,
    },
});
