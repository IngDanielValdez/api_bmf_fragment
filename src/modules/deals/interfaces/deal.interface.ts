import { Document } from 'mongoose';
import { BankNotify } from './bank-notify.interface';
import { ClosureReport } from './closure-report.interface';
import { DealHistory } from './deal-history.interface';

enum StatusType {
    rejected = 'rejected',
    accepted = 'accepted',
    onhold = 'onhold',
    replied = 'replied',
    approvednotfunded = 'approvednotfunded',
    completed = 'completed',
}

enum Territories { USA = 'USA', CANADA = 'CANADA', PR = 'PR', UK = 'UK' }

export interface Deal extends Document {
    companyID: string;
    code: string;
    banksIDs: BankNotify[];
    quantity: number;
    period: string;
    status: string;
    closingHistory: ClosureReport[];
    lifeHistory: DealHistory[];
    active: boolean;
    createdOn: Date;
    insertBy: string;
    lastSubmitted: Date;
    isRenovation: boolean;
    submittedBy: string;
    territories: Territories[];
}
