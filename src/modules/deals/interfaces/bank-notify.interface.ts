import { Document } from 'mongoose';

enum StatusType { rejected = 'rejected', accepted = 'accepted', onhold = 'onhold', replied = 'replied' }

export interface BankNotify {
    bankID: string;
    notify: boolean;
    status: string;
    approvedAmount: number;
    addedDate: Date;
    lastNotify: Date;
    updateDate: Date;
}
