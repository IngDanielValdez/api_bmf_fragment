import { Document } from 'mongoose';

export enum StatusType {
    notsent = 'notsent',
    rejected = 'rejected',
    accepted = 'accepted',
    onhold = 'onhold',
    sended = 'sended',
    replied = 'replied',
    approvednotfunded = 'approvednotfunded',
    completed = 'completed',
}

export interface DealHistory {
    status: string;
    statusReason: string;
    dateChange: string;
    insertBy: string;
}
