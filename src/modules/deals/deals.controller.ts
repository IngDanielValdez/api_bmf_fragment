import { Controller, Post, UseFilters, UseGuards, Req, Body,
     NotFoundException, ConflictException, BadRequestException, Get, Query, Param, Put, Logger } from '@nestjs/common';
import { DealsService } from './deals.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateDealDTO } from './DTO/create-deal.dto';
import { Deal } from './interfaces/deal.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { CompaniesService } from '../companies/companies.service';
import { DealHistory, StatusType } from './interfaces/deal-history.interface';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { PostmanService } from '../postman/postman.service';
import { BanksService } from '../banks/banks.service';
import { Postman } from '../postman/interfaces/postman.interface';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { AddBankDTO } from './DTO/add-bank.dto';
import { ChangeStatusDTO } from './DTO/change-status.dto';
import { CloseDealDTO } from './DTO/close-deal.dto';
import { UpdateNotifyDTO } from './DTO/update-notify.dto';
import { RangeQueryPipe } from '../../common/pipes/range-query.pipe';
import { CalendarService } from '../calendar/calendar.service';
import * as moment from 'moment';
import { Task } from '../calendar/interfaces/task.interface';
import { UsersService } from '../users/users.service';
import { MatchArrayQueryPipe } from '../../common/pipes/match-array-query.pipe';
import { AdminReport } from '../reports/interfaces/adm-report.interface';
import { ReportsService } from '../reports/reports.service';

@Controller('deals')
export class DealsController {
    constructor(private dealAPI: DealsService, private companyAPI: CompaniesService, private userAPI: UsersService,
                private PostmanAPI: PostmanService, private bankAPI: BanksService, private taskAPI: CalendarService,
                private reportAPI: ReportsService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getDeals(@Query(new MatchQueryPipe(['status', 'companyCode']), new RangeQueryPipe('lastSubmitted'),
                          new MatchArrayQueryPipe(['submittedBy'])) query):
    Promise<{ deals: Deal[], total: number }> {
        let deals;
        let total;
        if (query.state && query.state !== '') {
            deals = await this.dealAPI.getDeals(query, true);
            deals = deals.filter((deal: any) => {
                const exp = new RegExp(query.state.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'gi');
                if (exp.test(deal.companyID.state)) { return deal; }
            });
            total = 0;
        } else {
            deals = await this.dealAPI.getDeals(query);
            total = await this.dealAPI.countDeals(query);
        }

        return { deals, total };
    }

    @Get('export')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN']))
    async exportDeals(@Query(new MatchQueryPipe(['status', 'companyCode']), new RangeQueryPipe('lastSubmitted'),
                          new MatchArrayQueryPipe(['submittedBy'])) query): Promise<Deal[]> {

        let deals;
        if (query.state && query.state !== '') {
            deals = await this.dealAPI.exportDeals(query);
            deals = deals.filter((deal: any) => {
                const exp = new RegExp(query.state.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'gi');
                if (exp.test(deal.companyID.state)) { return deal; }
            });
        } else {
            deals = await this.dealAPI.exportDeals(query);
        }

        return deals;
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getDeal(@Param('id', new ValidateObjectIdPipe()) dealID: string): Promise<Deal> {
        const deal = await this.dealAPI.getDeal({ _id: dealID, active: true });
        if (!deal) { throw new NotFoundException('Deal not found', '6001'); }
        return deal;
    }

    @Put('/close/:id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async closeDeal(@Param('id', new ValidateObjectIdPipe()) dealID: string, @Body(new PickPipe(['bankID', 'closingAmount']),
                    new ValidateObjectIdPipe(['bankID'])) info: CloseDealDTO,
                    @Req() req): Promise<Deal> {
        const deal = await this.dealAPI.getDeal({ _id: dealID, active: true });
        const insertBy = req.token.user._id;

        if (!deal) { throw new NotFoundException('Deal not found', '6001'); }
        if (deal.status === 'completed') { throw new ConflictException('This deal is already closed', '6002'); }

        const lifeHistory: DealHistory = {
            statusReason: `${ (deal.submittedBy as any).name } has closed this deal.`,
            status: 'completed' as StatusType,
            dateChange: new Date().toISOString(),
            insertBy,
        };

        // add 3 Reminders
        try {
            const reminderDate = moment(new Date()).add(7, 'days');
            if (reminderDate.day() === 6) { reminderDate.add(2, 'days'); }
            if (reminderDate.day() === 7) { reminderDate.add(1, 'days'); }
            const description = 'Follow up on this deal, in case requires any additional loan. Verify if he is doing the payments.';
            const system = this.userAPI.SYSUSER._id.toString();
            // 7 Days
            let task = { title: 'SYS: Follow up reminder',
            description, assignTo: system, insertBy: system, reference: deal._id, reminderDate: reminderDate.toISOString() };
            await this.taskAPI.createTask(task);

            // 14 Days
            reminderDate.add(7, 'days');
            if (reminderDate.day() === 6) { reminderDate.add(2, 'days'); }
            if (reminderDate.day() === 7) { reminderDate.add(1, 'days'); }
            task = { title: 'SYS: Follow up reminder',
            description, assignTo: system, insertBy: system, reference: deal._id, reminderDate: reminderDate.toISOString() };
            await this.taskAPI.createTask(task);

            // 21 Days
            reminderDate.add(21, 'days');
            if (reminderDate.day() === 6) { reminderDate.add(2, 'days'); }
            if (reminderDate.day() === 7) { reminderDate.add(1, 'days'); }
            task = { title: 'SYS: Follow up reminder',
            description, assignTo: system, insertBy: system, reference: deal._id, reminderDate: reminderDate.toISOString() };
            await this.taskAPI.createTask(task);

        } catch (e) {
            Logger.log('The tasks could not be added to the calendar');
        }

        const report = { deal: deal._id, closingAmount: info.closingAmount, participants: [], insertBy };
        const reportDB = await this.reportAPI.createReport(report);
        return await this.dealAPI.updateDeal({ $push: { closingHistory: info, lifeHistory },
        lastClosingDate: new Date().toISOString(), status: StatusType.completed }, { _id: deal._id});
    }

    @Put('/changestatus/:id')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async changeStatus(@Req() req, @Param('id', new ValidateObjectIdPipe()) dealID: string, @Body() change: ChangeStatusDTO):
    Promise<{ deal: Deal }> {
        const deal = await this.dealAPI.getDeal({ _id: dealID, active: true });
        const insertBy = req.token.user._id;

        if (!deal) { throw new NotFoundException('Deal not found', '6001'); }

        if (deal.status === 'completed') { throw new ConflictException('This deal is closed', '6002'); }

        // Find the company
        const company = await this.companyAPI.getCompany({ _id: deal.companyID });
        if (!company) {
            throw new NotFoundException('Company not found', '5001');
        }

        // Prepair Update
        const lifeHistory: DealHistory = {
            statusReason: change.message,
            status: change.status as StatusType,
            dateChange: new Date().toISOString(),
            insertBy,
        };

        const update = { $push: { lifeHistory }, status: change.status };
        const dealDB = await this.dealAPI.updateDeal(update, { _id: deal._id });

        // const { name, code, taxid } = (deal.companyID as any);
        // const mail = {
        //     to: (deal.submittedBy as any).email || (deal.insertBy as any).email,
        //     subject: `Your deal has change to ${ change.status } - ${ name }`,
        //     body: `${ change.message }\n\nCompany: ${ name }\nCode: ${ code }\nTax ID:${ taxid }`,
        //     insertBy,
        // };

        // const postman = await this.PostmanAPI.createMail(mail);
        return { deal: dealDB };
    }

    @Put('/updatenotify')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async updateBankStatus(@Body(new ValidateObjectIdPipe(['notifyID'])) notify: UpdateNotifyDTO): Promise<any> {
        const update = { 'banksIDs.$.status': notify.status, 'banksIDs.$.approvedAmount': notify.approvedAmount,
        'banksIDs.$.updateDate': Date.now() };
        return await this.dealAPI.updateDeal(update, { 'banksIDs._id': notify.notifyID });
    }

    @Put('/send/:id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async resend(@Req() req, @Param('id', new ValidateObjectIdPipe()) dealID: string,
                 @Body(new PickPipe(['banksIDs', 'message']), new ValidateObjectIdPipe(['banksIDs'])) banks: AddBankDTO):
                 Promise<{ postman: Postman, deal: Deal}> {
        const deal = await this.dealAPI.getDeal({ _id: dealID, active: true });
        const period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
        const insertBy = req.token.user._id;

        if (!deal) { throw new NotFoundException('Deal not found', '6001'); }

        // Find the company
        const company = await this.companyAPI.getCompany({ _id: deal.companyID });
        if (!company) {
            throw new NotFoundException('Company not found', '5001');
        }

        if (company.blacklist) {
            throw new ConflictException('This company is blacklisted', '5003');
        }

        // Check if this deal has at least 3 account statements and 1 application
        const countStatements = company.bankStatements.filter(statement => statement.period === period);
        if (countStatements.length === 0 || !company.app) {
            throw new BadRequestException('Missing statements for this period', '6003');
        }

        // Exclude banks that are already inside
        const newBanks = [];
        banks.banksIDs.forEach((ID) => {
            if (deal.banksIDs.map((tmpBank: any) => tmpBank.bankID._id.toString()).indexOf(ID) === -1) {
                newBanks.push({ bankID: ID, addedDate: new Date().toISOString() });
            }
        });

        // Cancel, if there is no new bank.
        if (newBanks.length === 0) {
            throw new BadRequestException('No new bank has been added', '6004');
        }

        // Prepair Update
        const lifeHistory: DealHistory = {
            statusReason: `The deal has been forwarded to ${ newBanks.length } new banks.`,
            status: deal.status,
            dateChange: new Date().toISOString(),
            insertBy,
        };

        const update = { status: StatusType.sended, $push: { banksIDs: newBanks, lifeHistory } };
        const dealDB = await this.dealAPI.updateDeal(update, { _id: deal._id });

        // Enqueue this email
        const to = await this.bankAPI.getEmailBanks({ _id: { $in: banks.banksIDs } });
        const attachments = [];
        company.bankStatements.forEach((statement) => {
            if (statement.period === period) { attachments.push(statement.name); }
        });

        // Add Additional Accounts
        if (company.otheraccounts) {
            company.additionalAccounts.forEach((statement) => {
                if (statement.period === period) { attachments.push(statement.name); }
            });
        }

        // Add Card Processor Accounts
        if (company.cardprocessor) {
            company.cardStatements.forEach((statement) => {
                if (statement.period === period) { attachments.push(statement.name); }
            });
        }

        attachments.push(company.app);
        to.push(process.env.MAIL_AUTH_SENDER);
        const mail = {
            to,
            attachments,
            subject: `New Deal - ${ company.name } - #DL#${ company.code }`,
            body: `${ banks.message }\n\n${process.env.COMPANY_INFO}`,
            insertBy,
        };

        const postman = await this.PostmanAPI.createMail(mail);

        return { deal: dealDB, postman };
    }

    @Post()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async createDeal(@Req() req, @Body(new PickPipe(['companyID', 'quantity', 'territories', 'source']),
    new ValidateObjectIdPipe(['companyID'])) deal: CreateDealDTO): Promise<{ deal: Deal, postman: Postman, schedule: Task }> {
        const period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
        const insertBy = req.token.user._id;
        const lastSubmitted = new Date().toISOString();
        const submittedBy = insertBy;

        // Find the company
        const company = await this.companyAPI.getCompany({ _id: deal.companyID });
        if (!company) {
            throw new NotFoundException('Company not found', '5001');
        }

        if (company.blacklist) {
            throw new ConflictException('This company is blacklisted', '5003');
        }

        // Check if this deal has at least 3 account statements and 1 application
        const countStatements = company.bankStatements.filter(statement => statement.period === period);
        if (countStatements.length < 3 || !company.app) {
            throw new BadRequestException('Missing statements for this period', '6003');
        }

        // Check if this company have deal in this period
        const dealExist = await this.dealAPI.getDeal({ companyCode: company.code });
        if (dealExist && dealExist.period === period) {
            throw new ConflictException('This deal is blocked because it has an active period', '6005');
        }

        const lifeHistory: DealHistory = {
            status: StatusType.onhold,
            statusReason: `The deal was created, looking US$ ${ deal.quantity }.`,
            dateChange: new Date().toISOString(),
            insertBy,
        };

        let dealDetails;
        let dealDB;
        if (dealExist) {
            lifeHistory.statusReason =
            `The deal was reopen for a new period, looking for US$ ${ deal.quantity }.`;
            dealDetails = { lastSubmitted, period, $push: { lifeHistory }, submittedBy, banksIDs: [], quantity: deal.quantity,
            status: 'pending', territories: deal.territories };
            dealDB = await this.dealAPI.updateDeal(dealDetails, { _id: dealExist._id });
        } else {
            dealDetails = {
                lastSubmitted, period, insertBy, companyCode: company.code, lifeHistory: [lifeHistory], banksIDs: [], submittedBy, status: 'pending',
                territories: deal.territories,
            };
            Object.assign(deal, dealDetails);
            dealDB = await this.dealAPI.createDeal(deal);
        }

        // Enqueue this email
        const to = [process.env.MAIL_AUTH_SENDER];
        const attachments = [];
        company.bankStatements.forEach((statement) => {
            if (statement.period === period) {
                attachments.push(statement.name);
            }
        });

        // Add Additional Accounts
        if (company.otheraccounts) {
            company.additionalAccounts.forEach((statement) => {
                if (statement.period === period) { attachments.push(statement.name); }
            });
        }

        // Add Card Processor Accounts
        if (company.cardprocessor) {
            company.cardStatements.forEach((statement) => {
                if (statement.period === period) { attachments.push(statement.name); }
            });
        }

        attachments.push(company.app);
        const mail = {
            to,
            subject: `Deal pending review: ${ company.name } - #DL#${ company.code }`,
            body: `Visit the CRM to approve or deny this deal.\n\n${process.env.COMPANY_INFO}`,
            insertBy,
        };

        const reminderDate = moment(new Date()).add(2, 'days');
        if (reminderDate.day() === 6) { reminderDate.add(2, 'days'); }
        if (reminderDate.day() === 7) { reminderDate.add(1, 'days'); }

        const task = {
            title: 'Follow up reminder',
            description: 'Follow up on this deal',
            assignTo: submittedBy,
            insertBy: submittedBy,
            reference: dealDB._id,
            reminderDate: reminderDate.toISOString(),
        };

        let schedule: Task;
        try {
            schedule = await this.taskAPI.createTask(task);
            const tmpTask = {
                title: 'SYS: Follow up reminder',
                description: 'This deal must be closed by this date.',
                assignTo: this.userAPI.SYSUSER._id.toString(),
                insertBy: submittedBy,
                reference: dealDB._id,
                reminderDate: reminderDate.toISOString(),
            };
            await this.taskAPI.createTask(tmpTask);
        } catch (e) {
            Logger.log('The tasks could not be added to the calendar');
        }

        const postman = await this.PostmanAPI.createMail(mail);

        return { deal: dealDB, postman, schedule };
    }
}
