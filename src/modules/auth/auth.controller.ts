import { Controller, Post, Body, UnauthorizedException, Param, Get,
    NotFoundException, BadRequestException, GatewayTimeoutException } from '@nestjs/common';
import { LoginUserDTO } from './DTO/login-user.dto';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { PickPipe } from '../../common/pipes/pick.pipe';
import * as moment from 'moment';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { ActivationService } from '../activation/activation.service';

@Controller('auth')
export class AuthController {
    constructor(private userAPI: UsersService, private authAPI: AuthService, private activationAPI: ActivationService) {}

    @Post('login')
    async login(@Body(new PickPipe(['email', 'password'])) user: LoginUserDTO) {
        const userDB = await this.userAPI.getFullUser(user.email.toLowerCase().trim(), true);
        if (!userDB) { throw new UnauthorizedException('Invalid email or password', '0001'); }
        if (!this.authAPI.mathpassword(user.password, userDB.password)) {
            throw new UnauthorizedException('Invalid email or password', '0001');
        }
        if (!userDB.verifiedEmail) { throw new UnauthorizedException(`${ userDB._id.toString() }`, '0002'); }
        return { auth: this.authAPI.getToken(userDB) };
    }

    @Get(':id/verify/:hash')
    async verifyEmailUser(@Param('id', new ValidateObjectIdPipe()) userID: string, @Param('hash') hash: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }

        // Get Activation
        const activation = await this.activationAPI.getActivation({ hash });
        if (!activation) { throw new BadRequestException('Invalid activation token', '0004'); }

        if (activation.user.toString() !== user._id.toString()) { throw new BadRequestException('Invalid activation token', '0004'); }
        const now = moment(new Date());
        const expires = moment(activation.expiresOn);

        // Get difference between dates
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.activationAPI.deleteActivation({ hash });
            throw new BadRequestException('Token Expired', '0005');
        }

        // Update User
        await this.activationAPI.deleteActivation({ hash });
        const userDB = await this.userAPI.updateUser({ verifiedEmail: true } as any, user._id);
        return { auth: this.authAPI.getToken(userDB)};
    }

    @Get(':id/resend')
    async resendCode(@Param('id', new ValidateObjectIdPipe()) userID: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }
        const mail = await this.activationAPI.resendActivation(user);
        if (!mail) { throw new GatewayTimeoutException('Could not send mail', '9002'); }
        return { sended: true };
    }
}
