import { Controller, Get, UseFilters, UseGuards, Req } from '@nestjs/common';
import { DealsService } from '../deals/deals.service';
import { FeaturedEmployeeService } from '../featured-employee/featured-employee.service';
import { CalendarService } from '../calendar/calendar.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { CompaniesService } from '../companies/companies.service';
import * as moment from 'moment';
import { UsersService } from '../users/users.service';

@Controller('stats')
export class StatsController {
    constructor(private dealAPI: DealsService, private employeeAPI: FeaturedEmployeeService,
                private calendarAPI: CalendarService, private companyAPI: CompaniesService, private userAPI: UsersService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getEmployees(@Req() req): Promise<any> {
        const tokenUser = req.token.user;
        const today = new Date();

        // Employee of the week
        const startMonth = moment(today, 'YYYY-MM-DD').startOf('month');
        const endMonth = moment(today, 'YYYY-MM-DD').endOf('month');

        const employees = await this.employeeAPI.getFeaturedEmployees({ options: { active: true }, limit: 1 });

        const query = { options: { lastSubmitted: { $gte: startMonth, $lte: endMonth }, submittedBy: tokenUser._id } };

        // Company Stats
        const companies = await this.companyAPI.countCompanies({ options: {
            createdOn: { $gte: startMonth, $lte: endMonth }, insertBy: tokenUser._id },
        });

        // Tasks
        const $in = [tokenUser._id];
        if (tokenUser.role === 'ADMIN' || tokenUser.role === 'SUPERADMIN') { $in.push(this.userAPI.SYSUSER._id.toString()); }
        const tasks = await this.calendarAPI.getTasks({ options: {
            reminderDate: { $gte: startMonth, $lte: endMonth }, assignTo: { $in }, status: 'incomplete',
        } });

        // Deals Stats
        let total = 0;
        let closed = 0;
        let open = 0;
        const deals = await this.dealAPI.getDeals(query);
        if (deals) {
            total = deals.length;
            deals.forEach(deal => deal.status === 'completed' ? closed++ : open++);
        }

        // Earnings Stats
        const BiannualEarnings = await this.dealAPI.getDeals({ options: {
            // tslint:disable-next-line:object-literal-key-quotes
            lastSubmitted: { $gte: startMonth.subtract(6, 'months'), $lte: endMonth }, 'closingHistory.userID': tokenUser._id,
        } });

        const earnings = [];
        if (BiannualEarnings) {
            BiannualEarnings.forEach(deal => {
                deal.closingHistory.forEach(close => {
                    if (close.userID.toString() === tokenUser._id) {
                        earnings.push({ comission: close.commission, date: close.closingDate });
                    }
                });
            });
        }

        return { employeeOfTheWeek: employees.pop(), deals: { total, closed, open }, earnings, companies, tasks };
    }
}
