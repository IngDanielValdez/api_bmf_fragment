import { Module } from '@nestjs/common';
import { StatsController } from './stats.controller';
import { FeaturedEmployeeModule } from '../featured-employee/featured-employee.module';
import { DealsModule } from '../deals/deals.module';
import { CalendarModule } from '../calendar/calendar.module';
import { CompaniesModule } from '../companies/companies.module';
import { UsersModule } from '../users/users.module';

@Module({
  controllers: [StatsController],
  imports: [FeaturedEmployeeModule, DealsModule, CalendarModule, CompaniesModule, UsersModule],
})
export class StatsModule {}
