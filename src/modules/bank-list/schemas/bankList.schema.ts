import { Schema } from 'mongoose';

export const bankListSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 80,
        minlength: 1,
        trim: true,
        unique: true,
    },
    description: {
        type: String,
        required: [true, 'description is required'],
        maxlength: 300,
        trim: true,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    banksIDs: [{
        type: Schema.Types.ObjectId,
        ref: 'Bank',
        required: true,
    }],
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
