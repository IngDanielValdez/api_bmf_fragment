import { Document } from 'mongoose';

export interface BankList extends Document {
    name: string;
    description: string;
    insertBy: string;
    banksIDs: string[];
    createdOn: Date;
}
