import { Module } from '@nestjs/common';
import { BankListController } from './bank-list.controller';
import { BankListService } from './bank-list.service';
import { MongooseModule } from '@nestjs/mongoose';
import { bankListSchema } from './schemas/bankList.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'BankList', schema: bankListSchema }])],
  controllers: [BankListController],
  providers: [BankListService],
})
export class BankListModule {}
