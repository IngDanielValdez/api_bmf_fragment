import { IsNotEmpty, IsString, MaxLength, MinLength, IsArray } from 'class-validator';

export class CreateListDTO {
    @IsNotEmpty()
    @MaxLength(80)
    @MinLength(5)
    @IsString()
    name: string;

    @IsNotEmpty()
    @MaxLength(300)
    @IsString()
    description: string;

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(24, { each: true})
    @MinLength(24, { each: true })
    banksIDs: string[];
}
