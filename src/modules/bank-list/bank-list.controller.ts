import { Controller, Get, UseFilters, UseGuards, Param, NotFoundException, Body, Post, Req, Put, Delete } from '@nestjs/common';
import { BankListService } from './bank-list.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { BankList } from './interfaces/bank-list.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateListDTO } from './DTO/create-list.dto';
import { User } from '../users/interfaces/user.interface';

@Controller('bank-list')
export class BankListController {
    constructor(private bankListAPI: BankListService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getLists(): Promise<BankList[]> {
        return await this.bankListAPI.getLists();
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getList(@Param('id', new ValidateObjectIdPipe()) ListID: string): Promise<BankList> {
        const list = await this.bankListAPI.getList({ _id: ListID });
        if (!list) { throw new NotFoundException('List not found', 'B001'); }
        return list;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async createList(@Body(new PickPipe(['name', 'description', 'banksIDs']), new ValidateObjectIdPipe(['banksIDs'])) list: CreateListDTO,
                     @Req() req): Promise<BankList> {
        const tokenUser = req.token.user as User;
        Object.assign(list, { insertBy: tokenUser._id });
        return await this.bankListAPI.createList(list);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async updateList(@Param('id', new ValidateObjectIdPipe()) ListID: string,
                     @Body(new PickPipe(['name', 'description', 'banksIDs']), new ValidateObjectIdPipe(['banksIDs']))
    list: CreateListDTO): Promise<BankList> {
        return await this.bankListAPI.updateList(list, { _id: ListID });
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteList(@Param('id', new ValidateObjectIdPipe()) listID): Promise<BankList> {
        return await this.bankListAPI.deleteList(listID);
    }
}
