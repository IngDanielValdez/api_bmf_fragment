import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { BankList } from './interfaces/bank-list.interface';
import { InjectModel } from '@nestjs/mongoose';
import { CreateListDTO } from './DTO/create-list.dto';

@Injectable()
export class BankListService {
    constructor(@InjectModel('BankList') private BankListModel: Model<BankList>) {}

    async getLists(query?): Promise<BankList[]> {
        return await this.BankListModel.find()
        .populate('insertBy', 'name')
        .populate('banksIDs', 'name territories');
    }

    async getList(options: object): Promise<BankList> {
        return await this.BankListModel.findOne(options)
        .populate('insertBy', 'name')
        .populate('banksIDs', 'name territories');
    }

    async createList(list: BankList | CreateListDTO): Promise<BankList> {
        const listDB = new this.BankListModel(list);
        return await listDB.save();
    }

    async updateList(update, query: {}): Promise<BankList> {
        return await this.BankListModel.findOneAndUpdate(query, update, { new: true, runValidators: true, context: 'query' });
    }

    async deleteList(ID: string): Promise<BankList> {
        return await this.BankListModel.findOneAndDelete({ _id: ID });
    }
}
