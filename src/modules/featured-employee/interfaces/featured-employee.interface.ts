import { Document } from 'mongoose';

export interface FeaturedEmployee extends Document {
    userID: string;
    insertBy: string;
    quantity: number;
    photo: string;
}
