import { Schema } from 'mongoose';

export const FeaturedEmployeeSchema = new Schema({
    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    quantity: {
        type: Number,
        min: 0,
        required: true,
    },
    photo: {
        type: String,
    },
    username: {
        type: String,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    active: {
        type: Boolean,
        default: true,
    },
});
