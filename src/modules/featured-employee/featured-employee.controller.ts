import { Controller, Get, UseFilters, UseGuards, Query, Param, NotFoundException, Post, Req, Body, Put,
    Delete, UseInterceptors, UploadedFile, Res, BadRequestException } from '@nestjs/common';
import { FeaturedEmployeeService } from './featured-employee.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { FeaturedEmployee } from './interfaces/featured-employee.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateEmployeeDTO } from './DTO/create-employee.dto';
import { UsersService } from '../users/users.service';
import { UpdateEmployeeDTO } from './DTO/update-employee.dto';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { RangeQueryPipe } from '../../common/pipes/range-query.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { ValidFiledExtensionsInterceptor } from '../../common/filters/valid-filed-extensions.filter';
import { FilesService } from '../files/files.service';
import * as imagesizer from 'image-size';
import * as path from 'path';
import * as uuid from 'uuid';
import { GetPhotoDTO } from './DTO/get-photo.dto';
import { QueryGuard } from '../../common/guard/query.guard';

@Controller('featured-employee')
export class FeaturedEmployeeController {
    constructor(private employeeAPI: FeaturedEmployeeService, private userAPI: UsersService, private filesAPI: FilesService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getEmployees(@Query(new MatchQueryPipe(['username']), new RangeQueryPipe('createdOn')) query):
        Promise<{ employees: FeaturedEmployee[], total: number }> {
        const employees = await this.employeeAPI.getFeaturedEmployees(query);
        const total = await this.employeeAPI.countEmployees(query);
        return { employees, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getEmployee(@Param('id', new ValidateObjectIdPipe()) employeeID: string): Promise<FeaturedEmployee> {
        const employee = await this.employeeAPI.getFeaturedEmployee({ _id: employeeID });
        if (!employee) { throw new NotFoundException('Employee not found', '8001'); }
        return employee;
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async updateEmployee(@Param('id', new ValidateObjectIdPipe()) employeeID,
                         @Body(new PickPipe(['quantity'])) employee: UpdateEmployeeDTO): Promise <FeaturedEmployee> {
        return await this.employeeAPI.updateEmployee(employee, { _id: employeeID });
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteEmployee(@Param('id', new ValidateObjectIdPipe()) employeeID): Promise<FeaturedEmployee> {
        const employee: FeaturedEmployee = await this.getEmployee(employeeID);
        if (!employee) { throw new NotFoundException('Employee not found', '8001'); }
        const oldEmployee = this.employeeAPI.deleteEmployee({ _id: employeeID });
        let existFile = false;
        let dir;
        if (employee.photo) {
            dir = employee.photo.split('/');
            existFile = await this.filesAPI.fileExist(dir.slice(0, 2).join('/'), dir[dir.length - 1]);
            if (existFile) {
                await this.filesAPI.deleteFile(dir.slice(0, 2).join('/'), dir[dir.length - 1]);
            }
        }
        return oldEmployee;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async createEmployee(@Req() req, @Body(new ValidateObjectIdPipe(['userID']), new PickPipe(['quantity', 'userID'])) employee: CreateEmployeeDTO):
    Promise <FeaturedEmployee> {
        const user = await this.userAPI.getUser({ _id: employee.userID });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        const insertBy = req.token.user._id;
        Object.assign(employee, { insertBy, username: user.name });
        return await this.employeeAPI.createEmployee(employee);
    }

    @Put(':id/upload')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 2000000 }}),
    new ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png']))
    async uploadFile(@UploadedFile('file') file: Express.Multer.File, @Param('id', new ValidateObjectIdPipe()) id, @Res() res) {
        const employee: FeaturedEmployee = await this.getEmployee(id);
        const size: any = imagesizer.imageSize(file.buffer) ;
        if (size.height > 800 || size.width > 800) { throw new BadRequestException('Invalid image dimension', '8003'); }
        if (employee) {
            file.originalname = uuid.v4() + path.extname(file.originalname);
            const uploaded = await this.filesAPI.uploadFile(file, `employees/${ employee._id }/`);
            if (!uploaded) { throw new BadRequestException('Invalid File', '7004'); }
            const update = await this.employeeAPI.updateEmployee(({ photo: uploaded.Key }), { _id: employee._id });
            let existFile = false;
            let dir;
            if (employee.photo) {
                dir = employee.photo.split('/');
                existFile = await this.filesAPI.fileExist(dir.slice(0, 2).join('/'), dir[dir.length - 1]);
            }
            if (existFile) {
                await this.filesAPI.deleteFile(dir.slice(0, 2).join('/'), dir[dir.length - 1]);
                return res.json(update);
            } else {
                return res.json(update);
            }
        } else {
            throw new BadRequestException('Invalid Employee ID', '8002');
        }
    }

    @Get('file/download')
    @UseGuards(QueryGuard)
    async getFile(@Query() query: GetPhotoDTO, @Res() res) {
        const exist = await this.filesAPI.fileExist(query.path, query.file);
        if (!exist) { throw new NotFoundException('File not found', '7001'); }
        const file = await this.filesAPI.getFile(query.path, query.file);
        res.writeHead(200, {'Content-Type': file.ContentType });
        res.write( file.Body, 'binary');
        res.end(null, 'binary');
    }

}
