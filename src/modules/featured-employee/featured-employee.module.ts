import { Module } from '@nestjs/common';
import { FeaturedEmployeeService } from './featured-employee.service';
import { FeaturedEmployeeController } from './featured-employee.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { FeaturedEmployeeSchema } from './schemas/featured-employee.dto';
import { UsersModule } from '../users/users.module';
import { FilesModule } from '../files/files.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'FeaturedEmployee', schema: FeaturedEmployeeSchema }]), UsersModule, FilesModule],
  providers: [FeaturedEmployeeService],
  controllers: [FeaturedEmployeeController],
  exports: [FeaturedEmployeeService],
})
export class FeaturedEmployeeModule {}
