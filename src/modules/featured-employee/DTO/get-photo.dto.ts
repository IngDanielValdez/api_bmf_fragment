import { IsNotEmpty, IsString } from 'class-validator';

export class GetPhotoDTO {
    @IsString()
    @IsNotEmpty()
    path: string;

    @IsString()
    @IsNotEmpty()
    file: string;
}
