import { IsNumber, Min, IsNotEmpty } from 'class-validator';

export class UpdateEmployeeDTO {
    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    quantity: number;
}
