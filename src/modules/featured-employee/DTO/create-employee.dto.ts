import { IsString, IsNumber, Min, IsNotEmpty } from 'class-validator';

export class CreateEmployeeDTO {
    @IsNotEmpty()
    @IsString()
    userID: string;

    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    quantity: number;
}
