import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FeaturedEmployee } from './interfaces/featured-employee.interface';
import { CreateEmployeeDTO } from './DTO/create-employee.dto';

@Injectable()
export class FeaturedEmployeeService {
    constructor(@InjectModel('FeaturedEmployee') private FeaturedEmployeeModel: Model<FeaturedEmployee>) {}

    async getFeaturedEmployees(query?): Promise<FeaturedEmployee[]> {
        const { limit, skip, options } = query;
        return await this.FeaturedEmployeeModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate('userID', 'name')
        .sort({ createdOn: 'desc' });
    }

    async getFeaturedEmployee(options: object): Promise<FeaturedEmployee> {
        return await this.FeaturedEmployeeModel.findOne(options)
        .populate('userID', 'name');
    }

    async countEmployees(query): Promise<number> {
        const { options } = query;
        return await this.FeaturedEmployeeModel.countDocuments(options);
    }

    async createEmployee(employee: FeaturedEmployee | CreateEmployeeDTO): Promise<FeaturedEmployee> {
        const employeeDB = await new this.FeaturedEmployeeModel(employee);
        return await employeeDB.save();
    }

    async updateEmployee(update, query: {}): Promise<FeaturedEmployee> {
        return await this.FeaturedEmployeeModel.findOneAndUpdate(query, update, { new: true, runValidators: true, context: 'query' })
        .populate('userID', 'name');
    }

    async deleteEmployee(query) {
        return await this.FeaturedEmployeeModel.findOneAndDelete(query);
    }
}
