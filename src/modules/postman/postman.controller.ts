import { Controller, Get, UseFilters, UseGuards, Query, Param, NotFoundException } from '@nestjs/common';
import { PostmanService } from './postman.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { RangeQueryPipe } from '../../common/pipes/range-query.pipe';
import { Postman } from './interfaces/postman.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';

@Controller('postman')
export class PostmanController {
    constructor(private postmanAPI: PostmanService) { }

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getPostman(@Query(new RangeQueryPipe('createdOn')) query):
    Promise<{ postman: Postman[], total: number }> {
        const postman = await this.postmanAPI.getMails(query);
        const total = await this.postmanAPI.countMails(query);
        return { postman, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getMail(@Param('id', new ValidateObjectIdPipe()) id: string): Promise<Postman> {
        const postman = await this.postmanAPI.getMail({ _id: id });
        if (!postman) { throw new NotFoundException('Mail not found', '9001'); }
        return postman;
    }
}
