import { Module } from '@nestjs/common';
import { PostmanService } from './postman.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostmanSchema } from './schemas/postman.schema';
import { PostmanController } from './postman.controller';
import { SenderController } from './sender.controller';
import { SenderService } from './sender.service';
import { senderSchema } from './schemas/sender.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Postman', schema: PostmanSchema }]),
            MongooseModule.forFeature([{ name: 'Sender', schema: senderSchema }])],
  providers: [PostmanService, SenderService],
  exports: [PostmanService, SenderService],
  controllers: [PostmanController, SenderController],
})
export class PostmanModule {}
