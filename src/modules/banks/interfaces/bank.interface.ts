import { Document } from 'mongoose';

export interface Bank extends Document {
    name: string;
    acronym: string;
    contactName: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phone: string[];
    email: string[];
    territories: string[];
    classification: string[];
    insertBy: string;
    active: boolean;
    moreThan10k: boolean;
    lessThan10k: boolean;
    createdOn: Date;
}
