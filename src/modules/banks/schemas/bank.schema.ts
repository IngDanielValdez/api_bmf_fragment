import { Schema } from 'mongoose';

export const bankSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 120,
        minlength: 1,
        trim: true,
        unique: true,
    },
    acronym: {
        type: String,
        required: [true, 'acronym is required'],
        maxlength: 20,
        trim: true,
    },
    contactName: {
        type: String,
        required: [true, 'contact name is required'],
        maxlength: 120,
        minlength: 3,
        trim: true,
    },
    address: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    city: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    state: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    zip: {
        type: String,
        trim: true,
        maxlength: 9,
    },
    phone: [{
        type: String,
        trim: true,
        maxlength: 15,
        required: true,
    }],
    email: [{
        type: String,
        required: [true, 'email is required'],
        unique: true,
        maxlength: 60,
        trim: true,
        lowercase: true,
    }],
    territories: [{
        type: String,
        enum: ['USA', 'CANADA', 'PR', 'UK'],
        required: true,
    }],
    classification: [{
        type: String,
        enum: ['A', 'B', 'C', 'D'],
        required: true,
    }],
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    moreThan10k: {
        type: Boolean,
    },
    lessThan10k: {
        type: Boolean,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
