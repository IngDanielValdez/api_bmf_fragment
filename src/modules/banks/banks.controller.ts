import { Controller, Get, UseGuards, Query, Post, UseFilters,
    Body, Req, Put, Param, NotFoundException, Delete, UseInterceptors, InternalServerErrorException } from '@nestjs/common';
import { BanksService } from './banks.service';
import { AuthGuard } from '../../common/guard/auth.guard';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { RoleGuard } from '../../common/guard/role.guard';
import { Bank } from './interfaces/bank.interface';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateBankDTO } from './DTO/create-bank.dto';
import { User } from '../users/interfaces/user.interface';
import { UpdateBankDTO } from './DTO/update-bank.dto';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { MatchArrayQueryPipe } from '../../common/pipes/match-array-query.pipe';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';

@Controller('banks')
export class BanksController {
    constructor(private bankAPI: BanksService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getBanks(@Query(new MatchQueryPipe(['name', 'acronym', 'email', 'moreThan10k', 'contactName']),
    new MatchArrayQueryPipe(['territories', 'classification'])) query): Promise<{ banks: Bank[], total: number }> {
        const banks = await this.bankAPI.getBanks(query);
        const total = await this.bankAPI.countBanks(query);
        return { banks, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getBank(@Param('id', new ValidateObjectIdPipe()) bankID: string): Promise<Bank> {
        const bank = await this.bankAPI.getBank({ _id: bankID, active: true });
        if (!bank) { throw new NotFoundException('Bank not found', '3001'); }
        return bank;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async createBank(@Body(new PickPipe(['name', 'acronym', 'contactName', 'address',
    'city', 'state', 'zip', 'phone', 'email', 'territories', 'classification', 'moreThan10k', 'lessThan10k'])) bank: CreateBankDTO,
                     @Req() req): Promise<Bank> {
        const tokenUser = req.token.user as User;
        Object.assign(bank, { insertBy: tokenUser._id });
        return await this.bankAPI.createBank(bank);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async updateBank(@Param('id', new ValidateObjectIdPipe()) bankID: string,
                     @Body(new PickPipe(['name', 'acronym', 'contactName', 'address', 'city', 'state', 'zip',
                     'phone', 'email', 'territories', 'classification', 'moreThan10k', 'lessThan10k'])) bank: UpdateBankDTO): Promise<Bank> {
        return await this.bankAPI.updateBank(bank, bankID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteBank(@Param('id', new ValidateObjectIdPipe()) bankID): Promise<Bank> {
        return await this.bankAPI.deleteBank(bankID);
    }
}
