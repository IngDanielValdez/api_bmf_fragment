import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, IsEnum, Length, IsArray, IsBoolean, IsOptional } from 'class-validator';

export class CreateBankDTO {
    @IsNotEmpty()
    @MaxLength(120)
    @MinLength(1)
    @IsString()
    name: string;

    @IsNotEmpty()
    @MaxLength(20)
    @IsString()
    acronym: string;

    @IsNotEmpty()
    @MaxLength(120)
    @MinLength(3)
    @IsString()
    contactName: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    address: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    city: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    state: string;

    @IsNotEmpty()
    @Length(5)
    @IsString()
    zip: string;

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(15, { each: true})
    @MinLength(10, { each: true })
    phone: string[];

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(60, { each: true})
    @IsEmail({}, { each: true })
    email: string[];

    @IsArray()
    @IsNotEmpty()
    @IsEnum(['USA', 'CANADA', 'PR', 'UK'], { each: true })
    territories: string[];

    @IsArray()
    @IsNotEmpty()
    @IsEnum(['A', 'B', 'C', 'D'], { each: true })
    classification: string[];

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    moreThan10k: boolean;

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    lessThan10k: boolean;
}
