import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, IsEnum, Length, IsArray, IsBoolean, IsOptional } from 'class-validator';

export class UpdateBankDTO {
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(120)
    @MinLength(1)
    @IsString()
    name: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(20)
    @IsString()
    acronym: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(120)
    @MinLength(3)
    @IsString()
    contactName: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    address: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    city: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    state: string;

    @IsOptional()
    @IsNotEmpty()
    @Length(5)
    @IsString()
    zip: string;

    @IsOptional()
    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(15, { each: true})
    @MinLength(10, { each: true })
    phone: string[];

    @IsOptional()
    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(60, { each: true})
    @IsEmail({}, { each: true })
    email: string[];

    @IsOptional()
    @IsArray()
    @IsNotEmpty()
    @IsEnum(['USA', 'CANADA', 'PR', 'UK'], { each: true })
    territories: string[];

    @IsOptional()
    @IsArray()
    @IsNotEmpty()
    @IsEnum(['A', 'B', 'C', 'D'], { each: true })
    classification: string[];

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    moreThan10k: boolean;

    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    lessThan10k: boolean;
}
