import { Injectable } from '@nestjs/common';
import { Bank } from './interfaces/bank.interface';
import { CreateBankDTO } from './DTO/create-bank.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UpdateBankDTO } from './DTO/update-bank.dto';

@Injectable()
export class BanksService {
    constructor(@InjectModel('Bank') private BankModel: Model<Bank>) {}

    async getBanks(query?): Promise<Bank[]> {
        const { limit, skip, options } = query;
        return await this.BankModel.find(options)
        .sort({ name: 1 })
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0);
    }

    async getEmailBanks(options): Promise<string[]> {
        const emails = [];
        const banks = await this.BankModel.find(options);
        banks.forEach(bank => bank.email.forEach(mail => emails.push(mail)));
        return emails;
    }

    async getBank(options: object): Promise<Bank> {
        return await this.BankModel.findOne(options)
        .populate('insertBy', 'name email role');
    }

    async countBanks(query): Promise<number> {
        const { options } = query;
        return await this.BankModel.countDocuments(options);
    }

    async createBank(bank: Bank | CreateBankDTO): Promise<Bank> {
        const bankDB = new this.BankModel(bank);
        return await bankDB.save();
    }

    async updateBank(bank: Bank | UpdateBankDTO, ID: string): Promise<Bank> {
        return await this.BankModel.findOneAndUpdate({ _id: ID}, bank, { new: true, runValidators: true, context: 'query' });
    }

    async deleteBank(ID: string): Promise<Bank> {
        return await this.BankModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
}
