import { IsNotEmpty, IsNumber, Min, IsArray, ValidateNested } from 'class-validator';
import { CreateParticipantDTO } from './create-participant.dto';

export class UpdateReportDTO {
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    participants: CreateParticipantDTO[];
}
