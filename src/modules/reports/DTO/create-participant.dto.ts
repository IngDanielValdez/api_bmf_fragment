import { IsNotEmpty, IsString, MaxLength, MinLength, IsEnum, IsNumber, Min } from 'class-validator';

export class CreateParticipantDTO {
    @IsNotEmpty()
    @MaxLength(24)
    @MinLength(24)
    @IsString()
    beneficiary: string;

    @IsNotEmpty()
    @IsEnum(['commision', 'psf'])
    concept: string;

    @IsNotEmpty()
    @IsEnum(['income', 'expense'])
    type: string;

    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    amount: number;
}
