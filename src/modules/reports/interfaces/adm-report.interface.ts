import { Participant } from './participant.interface';
import { Document } from 'mongoose';

export interface AdminReport extends Document {
    deal: string;
    createdOn: Date;
    participants: Participant[];
    insertBy: string;
    closingAmount: number;
    status: string;
}
