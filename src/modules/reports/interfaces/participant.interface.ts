import { Document } from 'mongoose';

export interface Participant extends Document {
    beneficiary: string;
    concept: string;
    type: string;
    status: string;
    amount: number;
}
