import { Module } from '@nestjs/common';
import { ReportsController } from './reports.controller';
import { ReportsService } from './reports.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AdmReportSchema } from './schemas/adm-report.schema';
import { PostmanModule } from '../postman/postman.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AdmReport', schema: AdmReportSchema }]),
    PostmanModule,
  ],
  controllers: [ReportsController],
  providers: [ReportsService],
  exports: [ReportsService],
})
export class ReportsModule {}
