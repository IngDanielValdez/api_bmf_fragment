import { Schema } from 'mongoose';
import { ParticipantSchema } from './participant.schema';

export const AdmReportSchema = new Schema({
    deal: {
        type: Schema.Types.ObjectId,
        ref: 'Deal',
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    participants: [ParticipantSchema],
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    closingAmount: {
        type: Number,
        required: true,
        min: 0,
    },
    status: {
        type: String,
        enum: {
            values: ['open', 'closed', 'waiting'],
            message: 'Invalid type',
        },
        default: 'open',
    },
});
