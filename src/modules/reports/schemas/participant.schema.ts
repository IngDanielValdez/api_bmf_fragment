import { Schema } from 'mongoose';

export const ParticipantSchema = new Schema({
    beneficiary: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    concept: {
        type: String,
        enum: {
            values: ['commision', 'psf'],
            message: 'Invalid concept',
        },
        default: 'commision',
    },
    type: {
        type: String,
        enum: {
            values: ['income', 'expense'],
            message: 'Invalid type',
        },
        default: 'income',
    },
    amount: {
        type: Number,
        required: true,
    },
    status: {
        type: String,
        enum: {
            values: ['waiting', 'accepted', 'rejected'],
            message: 'invalid status',
        },
        default: 'waiting',
    },
});
