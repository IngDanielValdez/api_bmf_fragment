import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AdminReport } from './interfaces/adm-report.interface';

@Injectable()
export class ReportsService {
    constructor(@InjectModel('AdmReport') private ReportModel: Model<AdminReport>) {}

    async getReports(query?, infinity = false): Promise<AdminReport[]> {
        const { limit, skip, options } = query;
        let limitPage = Math.abs((Number(limit) < 100 ? Number(limit) : 10)) || 10;
        if (infinity) { limitPage = undefined; }
        return await this.ReportModel.find(options)
        .limit(limitPage)
        .skip(Math.abs(Number(skip)) || 0)
        .populate('insertBy', 'email name role')
        .populate({
            path: 'deal',
            select: 'companyID code',
            populate: { path: 'companyID', select: 'name dba code' },
        })
        .populate('participants.beneficiary', 'name email role');
    }

    async countReports(query): Promise<number> {
        const { options } = query;
        return await this.ReportModel.countDocuments(options);
    }

    async getReport(options: object): Promise<AdminReport> {
        return await this.ReportModel.findOne(options)
        .populate('insertBy', 'email name role')
        .populate({
            path: 'deal',
            select: 'companyID code',
            populate: { path: 'companyID', select: 'name dba code' },
        })
        .populate('participants.beneficiary', 'name email role');
    }

    async createReport(report): Promise<AdminReport> {
        const reportDB = new this.ReportModel(report);
        return await reportDB.save();
    }

    async updateReport(update, query: {}): Promise<AdminReport> {
        return await this.ReportModel.findOneAndUpdate(query, update, { new: true, runValidators: true, context: 'query' })
        .populate('insertBy', 'email name role')
        .populate({
            path: 'deal',
            select: 'companyID code',
            populate: { path: 'companyID', select: 'name dba code' },
        })
        .populate('participants.beneficiary', 'name email role');
    }

    async deleteReport(ID: string): Promise<AdminReport> {
        return await this.ReportModel.findOneAndDelete({ _id: ID });
    }
}
