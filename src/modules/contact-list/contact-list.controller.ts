import { Controller, Get, UseFilters, UseGuards, Query, Param, NotFoundException, Post, Body, Delete, Put, Req } from '@nestjs/common';
import { ContactListService } from './contact-list.service';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { ContactList } from './interfaces/contact-list.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { User } from '../users/interfaces/user.interface';
import { CreateContactListDTO } from './DTO/create-contact-list.dto';
import { UpdateContactListDTO } from './DTO/update-contact-list.dto';
import { UsersService } from '../users/users.service';
import { ContactService } from './contact.service';

@Controller('contact-list')
export class ContactListController {
    constructor(private contactListAPI: ContactListService, private userAPI: UsersService, private contactAPI: ContactService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard)
    async getContactLists(@Req() req, @Query(new MatchQueryPipe(['name'])) query): Promise<{ contactLists: ContactList[], total: number }> {
        const tokenUser = req.token.user as User;
        if (tokenUser.role === 'USER') { Object.assign(query.options, { assignedTo: tokenUser._id }); }
        const contactLists = await this.contactListAPI.getContactLists(query);
        const total = await this.contactListAPI.countContactLists(query);
        return { contactLists, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getContactList(@Req() req, @Param('id', new ValidateObjectIdPipe()) contactListID: string): Promise<ContactList> {
        const tokenUser = req.token.user as User;
        const query = { _id: contactListID, active: true };
        if (tokenUser.role === 'USER') { Object.assign(query, { assignedTo: tokenUser._id }); }
        const contactList = await this.contactListAPI.getContactList(query);
        if (!contactList) { throw new NotFoundException('Contact list not found', 'F001'); }
        return contactList;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'ADMIN']))
    async createContactList(@Body(new PickPipe(['name', 'assignedTo'])) contactList: CreateContactListDTO, @Req() req): Promise<ContactList> {
        const tokenUser = req.token.user as User;
        const user = await this.userAPI.getUser({ _id: contactList.assignedTo });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        Object.assign(contactList, { insertBy: tokenUser._id });
        return await this.contactListAPI.createContactList(contactList);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async updateContactList(@Param('id', new ValidateObjectIdPipe()) contactListID: string,
                            @Body(new PickPipe(['name', 'assignedTo'])) contactList: UpdateContactListDTO): Promise<ContactList> {
        if (contactList.assignedTo && contactList.assignedTo !== '') {
            const user = await this.userAPI.getUser({ _id: contactList.assignedTo });
            if (!user) { throw new NotFoundException('User not found', '1001'); }
        }
        return await this.contactListAPI.updateContactList(contactList, contactListID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteContactList(@Param('id', new ValidateObjectIdPipe()) contactListID): Promise<ContactList> {
        const list = await this.contactListAPI.getContactList({ _id: contactListID, active: true });
        if (!list) { throw new NotFoundException('Contact list not found', 'F001'); }
        await this.contactAPI.deleteManyContact({ contactList: contactListID  });
        return await this.contactListAPI.deleteContactList(contactListID);
    }
}
