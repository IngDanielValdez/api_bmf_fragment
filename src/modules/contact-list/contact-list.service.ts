import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ContactList } from './interfaces/contact-list.interface';
import { CreateContactListDTO } from './DTO/create-contact-list.dto';
import { UpdateContactListDTO } from './DTO/update-contact-list.dto';

@Injectable()
export class ContactListService {
    constructor(@InjectModel('ContactList') private ContactListModel: Model<ContactList>) {}

    async getContactLists(query?): Promise<ContactList[]> {
        const { limit, skip, options } = query;
        return await this.ContactListModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .sort({ createdOn: 'desc' })
        .populate('assignedTo', 'name');
    }

    async countContactLists(query): Promise<number> {
        const { options } = query;
        return await this.ContactListModel.countDocuments(options);
    }

    async getContactList(options: object): Promise<ContactList> {
        return await this.ContactListModel.findOne(options)
        .populate('assignedTo', 'name');
    }

    async createContactList(contactList: ContactList | CreateContactListDTO): Promise<ContactList> {
        const contactListDB = new this.ContactListModel(contactList);
        return await contactListDB.save();
    }

    async updateContactList(contactList: ContactList | UpdateContactListDTO, ID: string): Promise<ContactList> {
        return await this.ContactListModel.findOneAndUpdate({ _id: ID}, contactList, { new: true, runValidators: true, context: 'query' });
    }

    async deleteContactList(ID: string): Promise<ContactList> {
        return await this.ContactListModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
}
