import { Module } from '@nestjs/common';
import { ContactListController } from './contact-list.controller';
import { ContactListService } from './contact-list.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ContactListSchema } from './schemas/contact-list.schema';
import { ContactSchema } from './schemas/contact.schema';
import { ContactService } from './contact.service';
import { ContactController } from './contact.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'ContactList', schema: ContactListSchema }]),
    MongooseModule.forFeature([{ name: 'Contact', schema: ContactSchema }]),
  ],
  controllers: [ContactListController, ContactController],
  providers: [ContactListService, ContactService],
})
export class ContactListModule {}
