import { IsString, MaxLength, MinLength, IsMongoId, IsOptional } from 'class-validator';

export class UpdateContactListDTO {
    @IsOptional()
    @MaxLength(30)
    @MinLength(1)
    @IsString()
    name: string;

    @IsOptional()
    @IsMongoId()
    assignedTo: string;
}
