import { IsNotEmpty, IsString, MaxLength, MinLength, IsMongoId } from 'class-validator';

export class CreateContactDTO {
    @MaxLength(120)
    @IsString()
    name: string;

    @MaxLength(120)
    @IsString()
    companyName: string;

    @MaxLength(120)
    @IsString()
    address: string;

    @MaxLength(15)
    @IsString()
    state: string;

    @MaxLength(15)
    @IsString()
    zip: string;

    @MaxLength(20)
    @IsString()
    phone: string;

    @MaxLength(30)
    @IsString()
    email: string;

    @IsMongoId()
    @IsNotEmpty()
    contactList: string;
}
