import { IsNotEmpty, IsString, MaxLength, MinLength, IsMongoId, IsArray, ValidateNested } from 'class-validator';
import { CreateContactDTO } from './create-contact.dto';

export class CreateContactBulkDTO {
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    contacts: CreateContactDTO[];
 }
