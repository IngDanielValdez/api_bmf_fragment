import { IsNotEmpty, IsString, MaxLength, MinLength, IsMongoId, IsOptional, IsBoolean } from 'class-validator';

export class UpdateContactDTO {
    @IsOptional()
    @MaxLength(120)
    @IsString()
    name: string;

    @IsOptional()
    @MaxLength(120)
    @IsString()
    companyName: string;

    @IsOptional()
    @MaxLength(120)
    @IsString()
    address: string;

    @IsOptional()
    @MaxLength(15)
    @IsString()
    state: string;

    @IsOptional()
    @MaxLength(15)
    @IsString()
    zip: string;

    @IsOptional()
    @MaxLength(20)
    @IsString()
    phone: string;

    @IsOptional()
    @MaxLength(30)
    @IsString()
    email: string;

    @IsOptional()
    @IsMongoId()
    contactList: string;

    @IsOptional()
    @IsBoolean()
    recall: boolean;
}
