import { MaxLength, MinLength, IsNotEmpty, IsString } from 'class-validator';

export class MakeCallDTO {
    @IsNotEmpty()
    @MaxLength(300)
    @MinLength(1)
    @IsString()
    note: string;
}
