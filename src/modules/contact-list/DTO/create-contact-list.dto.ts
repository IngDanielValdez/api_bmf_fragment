import { IsNotEmpty, IsString, MaxLength, MinLength, IsMongoId } from 'class-validator';

export class CreateContactListDTO {
    @IsNotEmpty()
    @MaxLength(30)
    @MinLength(1)
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsMongoId()
    assignedTo: string;
}
