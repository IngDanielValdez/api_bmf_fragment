import { Controller, Get, UseFilters, UseGuards, Query, Param, NotFoundException, Post, Body, Delete, Put, Req, Logger } from '@nestjs/common';
import { ContactListService } from './contact-list.service';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { User } from '../users/interfaces/user.interface';
import { UsersService } from '../users/users.service';
import { ContactService } from './contact.service';
import { Contact } from './interfaces/contact.interface';
import { CreateContactDTO } from './DTO/create-contact.dto';
import { UpdateContactDTO } from './DTO/update-contact.dto';
import { CreateContactBulkDTO } from './DTO/create-contact-bulk.dto';
import * as _ from 'underscore';
import { MakeCallDTO } from './DTO/make-call.dto';
@Controller('contact')
export class ContactController {
    constructor(private contactAPI: ContactService, private userAPI: UsersService, private contactListAPI: ContactListService) {}

    @Get(':id')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard)
    async getContacts(@Param('id') contactListID: string,
                      @Req() req, @Query(new MatchQueryPipe(['name'])) query): Promise<{ contacts: Contact[], total: number }> {
        const tokenUser = req.token.user as User;
        if (query.recall && (query.recall === 'true' || query.recall === true)) {
            Object.assign(query.options, { recall: true });
        }
        const list = await this.contactListAPI.getContactList({ _id: contactListID, active: true });
        if (!list) { throw new NotFoundException('Contact list not found', 'F001'); }
        Object.assign(query.options, { contactList: list._id });
        const contacts = await this.contactAPI.getContacts(query);
        const total = await this.contactAPI.countContacts(query);
        return { contacts, total };
    }

    @Get(':id/detail')
    @UseGuards(AuthGuard)
    async getContact(@Req() req, @Param('id', new ValidateObjectIdPipe()) contactID: string): Promise<Contact> {
        const tokenUser = req.token.user as User;
        const query = { _id: contactID, active: true };
        const contact = await this.contactAPI.getContact(query);
        if (!contact) { throw new NotFoundException('Contact not found', 'F002'); }
        return contact;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'ADMIN']))
    async createContact(@Body(new PickPipe(['name', 'companyName', 'address', 'state',
    'zip', 'phone', 'email', 'contactList'])) contact: CreateContactDTO,
                        @Req() req): Promise<Contact> {
        const tokenUser = req.token.user as User;
        Object.assign(contact, { insertBy: tokenUser._id });
        return await this.contactAPI.createContact(contact);
    }

    @Post('bulk')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'ADMIN']))
    async createContactBulk(@Body(new PickPipe(['contacts'])) bulk: CreateContactBulkDTO, @Req() req): Promise<Contact> {
        const tokenUser = req.token.user as User;
        const contacts = [];
        bulk.contacts.forEach(contact => {
            if (contact.contactList && contact.contactList !== '') {
                const tmpContact = _.pick(contact, ['name', 'companyName', 'address', 'state', 'zip', 'phone', 'email', 'contactList']);
                Object.assign(tmpContact, { insertBy: tokenUser._id });
                contacts.push(tmpContact);
            }
        });

        return await this.contactAPI.insertManyContacts(contacts);
    }

    @Put(':id/makecall')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard)
    async makeCall(@Param('id', new ValidateObjectIdPipe()) contactID: string,
                   @Body(new PickPipe(['note'])) call: MakeCallDTO): Promise<Contact> {
        const contact = await this.contactAPI.getContact({ _id: contactID });
        if (!contact) { throw new NotFoundException('Contact not found', '3001'); }
        return await this.contactAPI.updateContact({ $push: { calls: call } }, contactID);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard)
    async updateContact(@Param('id', new ValidateObjectIdPipe()) contactID: string,
                        @Body(new PickPipe(['name', 'companyName', 'address', 'state', 'recall',
                            'zip', 'phone', 'email', 'contactList'])) contact: UpdateContactDTO): Promise<Contact> {
        if (contact.contactList && contact.contactList !== '')  {
            const list = await this.contactListAPI.getContactList({ _id: contact.contactList, active: true });
            if (!list) { throw new NotFoundException('Contact list not found', 'F001'); }
        }
        return await this.contactAPI.updateContact(contact, contactID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteContact(@Param('id', new ValidateObjectIdPipe()) contactID): Promise<Contact> {
        return await this.contactAPI.deleteContact({ _id: contactID });
    }
}
