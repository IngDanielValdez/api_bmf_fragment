import { Schema } from 'mongoose';

export const ContactListSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 30,
        trim: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    assignedTo: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});
