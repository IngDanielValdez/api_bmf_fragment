import { Schema } from 'mongoose';

export const ContactSchema = new Schema({
    name: {
        type: String,
        maxlength: 120,
        trim: true,
    },
    companyName: {
        type: String,
        maxlength: 120,
        trim: true,
    },
    address: {
        type: String,
        maxlength: 120,
        trim: true,
    },
    state: {
        type: String,
        maxlength: 15,
        trim: true,
    },
    zip: {
        type: String,
        maxlength: 15,
        trim: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    phone: {
        type: String,
    },
    email: {
        type: String,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    contactList: {
        type: Schema.Types.ObjectId,
        ref: 'ContactList',
        required: true,
    },
    calls: [{
        date: {
            type: Date,
            default: Date.now,
        },
        note: {
            type: String,
            trim: true,
            maxlength: 300,
        },
    }],
    recall: {
        type: Boolean,
        default: false,
    },
});
