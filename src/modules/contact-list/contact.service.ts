import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Contact } from './interfaces/contact.interface';
import { CreateContactDTO } from './DTO/create-contact.dto';
import { UpdateContactDTO } from './DTO/update-contact.dto';

@Injectable()
export class ContactService {
    constructor(@InjectModel('Contact') private ContactModel: Model<Contact>) {}

    async getContacts(query?): Promise<Contact[]> {
        const { limit, skip, options } = query;
        return await this.ContactModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .sort({ calls: 0 });
    }

    async countContacts(query): Promise<number> {
        const { options } = query;
        return await this.ContactModel.countDocuments(options);
    }

    async getContact(options: object): Promise<Contact> {
        return await this.ContactModel.findOne(options);
    }

    async createContact(contact: Contact | CreateContactDTO): Promise<Contact> {
        const contactDB = new this.ContactModel(contact);
        return await contactDB.save();
    }

    async updateContact(contact, ID: string): Promise<Contact> {
        return await this.ContactModel.findOneAndUpdate({ _id: ID}, contact, { new: true, runValidators: true, context: 'query' });
    }

    async deleteContact(query): Promise<Contact> {
        return await this.ContactModel.findOneAndDelete(query);
    }

    async deleteManyContact(query): Promise<any> {
        return await this.ContactModel.deleteMany(query);
    }

    async insertManyContacts(query): Promise<any> {
        return await this.ContactModel.insertMany(query);
    }
}
