import { Document } from 'mongoose';
import { User } from 'src/modules/users/interfaces/user.interface';

export interface ContactList extends Document {
    name: string;
    active: boolean;
    createdOn: Date;
    insertBy: User;
    assignedTo: User;
 }
