import { Document } from 'mongoose';
import { User } from 'src/modules/users/interfaces/user.interface';
import { ContactList } from './contact-list.interface';

export interface Contact extends Document {
    name: string;
    companyName: string;
    address: string;
    state: string;
    zip: string;
    active: boolean;
    createdOn: Date;
    phone: string;
    email: string;
    insertBy: User;
    contactList: ContactList[];
    calls: Call[];
    recall: boolean;
 }

interface Call extends Document {
    date: Date;
    note: string;
}
