import { Document } from 'mongoose';

export interface User extends Document {
    role: string;
    name: string;
    email: string;
    verifiedEmail: boolean;
    password: string;
    active: boolean;
    createdOn: Date;
    uuid: string;
    photo: string;
    lastActivity?: Date;
}
