import { Controller, Get, UseGuards, Query, Post, UseFilters, Body, ConflictException,
    InternalServerErrorException, NotFoundException, Param, BadRequestException, GatewayTimeoutException,
    Put, Req, UnauthorizedException, Delete, UseInterceptors, UploadedFile, Res } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { User } from './interfaces/user.interface';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { UuidPipe } from '../../common/pipes/uuid.pipe';
import { PasswordEncryptPipe } from '../../common/pipes/password-encrypt.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateUserDTO } from './DTO/create-user.dto';
import { ActivationService } from '../activation/activation.service';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import * as moment from 'moment';
import { UpdateUserDTO } from './DTO/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ValidFiledExtensionsInterceptor } from '../../common/filters/valid-filed-extensions.filter';
import * as imagesizer from 'image-size';
import * as path from 'path';
import * as uuid from 'uuid';
import { QueryGuard } from '../../common/guard/query.guard';
import { GetPhotoDTO } from '../featured-employee/DTO/get-photo.dto';
import { FilesService } from '../files/files.service';
import { AuthService } from '../auth/auth.service';

@Controller('users')
export class UsersController {
    constructor(private userAPI: UsersService, private activationAPI: ActivationService,
                private filesAPI: FilesService, private authAPI: AuthService) {}

    @Get()
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getUsers(@Query(new MatchQueryPipe(['name', 'email', 'role'])) query): Promise<User[]> {
        return await this.userAPI.getUsers(query);
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getUser(@Param('id', new ValidateObjectIdPipe()) userID: string, @Req() req): Promise<User> {
        // Only ADMIN can check other users
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== userID) {
            throw new UnauthorizedException('insufficient permissions');
        }

        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        return user;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async createUser(@Body(new UuidPipe(), new PasswordEncryptPipe(),
                     new PickPipe(['role', 'name', 'email', 'password', 'uuid'])) user: CreateUserDTO): Promise<User> {
        const existUser = await this.userAPI.getUser({ email: user.email });
        if (existUser) { throw new ConflictException('This user already exist', '1002'); }
        const userDB = await this.userAPI.createUser(user);
        if (!userDB) { throw new InternalServerErrorException('Internal server error'); }
        const activation = await this.activationAPI.createActivation(userDB);
        await this.activationAPI.sendActivation(userDB, activation);
        return userDB;
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteUser(@Param('id', new ValidateObjectIdPipe()) userID): Promise<User> {
        return await this.userAPI.deleteUser(userID);
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    async updateUser(@Param('id', new ValidateObjectIdPipe()) userID: string,
                     @Body(new PickPipe(['name'])) user: UpdateUserDTO, @Req() req): Promise<User> {

        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== userID) {
            throw new UnauthorizedException('insufficient permissions');
        }
        return await this.userAPI.updateUser(user, userID);
    }

    @Get(':id/verify/:hash')
    async verifyEmailUser(@Param('id', new ValidateObjectIdPipe()) userID: string, @Param('hash') hash: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }

        // Get Activation
        const activation = await this.activationAPI.getActivation({ hash });
        if (!activation) { throw new BadRequestException('Invalid activation token', '0004'); }

        if (activation.user.toString() !== user._id.toString()) { throw new BadRequestException('Invalid activation token', '0004'); }
        const now = moment(new Date());
        const expires = moment(activation.expiresOn);

        // Get difference between dates
        const diff = expires.diff(now, 'minutes');
        if (diff <= 0) {
            await this.activationAPI.deleteActivation({ hash });
            throw new BadRequestException('Token Expired', '0005');
        }

        // Update User
        await this.activationAPI.deleteActivation({ hash });
        const userDB = this.userAPI.updateUser({ verifiedEmail: true } as User, user._id);
        return userDB;
    }

    @Get(':id/resend')
    async resendCode(@Param('id', new ValidateObjectIdPipe()) userID: string) {
        // Get User
        const user = await this.userAPI.getUser({ _id: userID, active: true });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        if (user.verifiedEmail) { throw new BadRequestException('This user is already verified', '0003'); }
        const mail = await this.activationAPI.resendActivation(user);
        if (!mail) { throw new GatewayTimeoutException('Could not send mail', '9002'); }
        return { sended: true };
    }

    @Put(':id/upload')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 2000000 }}),
    new ValidFiledExtensionsInterceptor(['image/jpeg', 'image/png']))
    async uploadFile(@UploadedFile('file') file: Express.Multer.File, @Param('id', new ValidateObjectIdPipe()) id, @Req() req, @Res() res) {
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== id) {
            throw new UnauthorizedException('insufficient permissions');
        }

        const user: User = await this.userAPI.getUser({ _id: id });
        const size: any = imagesizer.imageSize(file.buffer) ;
        if (size.height > 800 || size.width > 800) { throw new BadRequestException('Invalid image dimension', '4002'); }
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        file.originalname = uuid.v4() + path.extname(file.originalname);
        const uploaded = await this.filesAPI.uploadFile(file, `users/${ user._id }/`);
        if (!uploaded) { throw new BadRequestException('Invalid File', '7004'); }

        const update = await this.userAPI.updateUser(({ photo: uploaded.Key } as any), user._id.toString());
        let existFile = false;

        if (!user.photo) { return await res.json({ user: update, auth: this.authAPI.getToken(update) }); }

        const dir = user.photo.split('/');
        existFile = await this.filesAPI.fileExist(dir.slice(0, 2).join('/'), dir[dir.length - 1]);

        if (existFile) {
            await this.filesAPI.deleteFile(dir.slice(0, 2).join('/'), dir[dir.length - 1]);
            return await res.json({ user: update, auth: this.authAPI.getToken(update) });
        }

        return await res.json({ user: update, auth: this.authAPI.getToken(update) });
    }

    @Get('file/download')
    @UseGuards(QueryGuard)
    async getFile(@Query() query: GetPhotoDTO, @Res() res) {
        const exist = await this.filesAPI.fileExist(query.path, query.file);
        if (!exist) { throw new NotFoundException('File not found', '7001'); }
        const file = await this.filesAPI.getFile(query.path, query.file);
        res.writeHead(200, {'Content-Type': file.ContentType });
        res.write( file.Body, 'binary');
        res.end(null, 'binary');
    }
}
