import { IsNotEmpty, IsString, MaxLength, IsOptional } from 'class-validator';

export class UpdateUserDTO {
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    name: string;
}
