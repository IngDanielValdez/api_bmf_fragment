import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, IsEnum } from 'class-validator';

export class CreateUserDTO {
    @IsNotEmpty()
    @IsEnum(['ADMIN', 'MANAGER', 'USER', 'SUPPORT', 'ANALYST'])
    role: string;

    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @MinLength(8)
    password: string;
}
