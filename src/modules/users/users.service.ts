import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './DTO/create-user.dto';
import { UpdateUserDTO } from './DTO/update-user.dto';

@Injectable()
export class UsersService {
    SYSUSER = { _id: '5d9d587d557b3a09646b6af3' };
    constructor(@InjectModel('User') private UserModel: Model<User>) {
        this.init();
    }

    async init() {
        this.SYSUSER = await this.getUser({ role: 'SYSTEM' });
        if (!this.SYSUSER) {
            this.SYSUSER = await this.createUser({ name: 'SYSTEM', email: 'system@businessmarketfinders.com', password: 'SYSTEM', role: 'SYSTEM' });
        }
    }

    async getUsers(query?): Promise<User[]> {
        const { limit, skip, options } = query;
        return await this.UserModel.find(options)
        .ne('role', 'SYSTEM')
        .skip(Math.abs(Number(skip)) || 0);
    }

    async getUser(options: object): Promise<User> {
        return await this.UserModel.findOne(options);
    }

    async getFullUser(email: string, active?: boolean): Promise<User> {
        return await this.UserModel.findOne({ email, active }).select('+password');
    }

    async createUser(user: User | CreateUserDTO): Promise<User> {
        const userDB = new this.UserModel(user);
        return await userDB.save();
    }

    async updateUser(user: UpdateUserDTO | User, ID: string): Promise<User> {
        return await this.UserModel.findOneAndUpdate({ _id: ID}, user, { new: true, runValidators: true, context: 'query' });
    }

    async deleteUser(ID: string): Promise<User> {
        return await this.UserModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
}
