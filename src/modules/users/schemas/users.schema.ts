import { Schema } from 'mongoose';

export const UserSchema = new Schema({
    role: {
        type: String,
        enum: {
            values: ['SUPERADMIN', 'MANAGER', 'ADMIN', 'USER', 'SUPPORT', 'ANALYST', 'SYSTEM'],
            message: 'Invalid type user',
        },
        default: 'USER',
    },
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 30,
        trim: true,
    },
    email: {
        type: String,
        required: [true, 'email is required'],
        unique: true,
        maxlength: 60,
        trim: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: [true, 'password is required'],
        trim: true,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    photo: {
        type: String,
        required: false,
        trim: true,
    },
    verifiedEmail: {
        type: Boolean,
        default: false,
    },
    lastActivity: {
        type: Date,
    },
});
