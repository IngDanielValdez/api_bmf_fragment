import { Injectable, Logger } from '@nestjs/common';
import * as notifier from 'mail-notifier';
import { DealsService } from '../deals/deals.service';

@Injectable()
export class MailListenerService {
    constructor(private dealAPI: DealsService) {}
    pop3;
    imap = {
        user: 'deals@businessmarketfinders.com',
        password: 'Sabrosura215',
        host: 'imap.secureserver.net',
        port: 993,
        tls: true,
        tlsOptions: { rejectUnauthorized: false },
    };

    async banksListener() {
        this.pop3 = await notifier(this.imap);
        this.pop3.on('mail', (mail) => this.checkSubjects(mail));
        this.pop3.on('stop', () => {
            Logger.log('Closed by stop');
            this.reconnect();
        });
        this.pop3.on('error', () => {
            Logger.log('Closed by error');
            this.reconnect();
        });
        this.pop3.start();
    }

    async reconnect() {
        Logger.log('Reconnect');
        this.pop3 = await notifier(this.imap);
        this.banksListener();
    }

    async checkSubjects(mail) {
        if (!mail.subject) { return true; }
        const hastag = mail.subject.indexOf('#DL#');

        if (hastag === -1) { return true; }
        const code = await this.getCode(mail.subject);
        const deal = await this.dealAPI.getDeal({ companyCode: code });

        if (!deal) { return true; }

        let foundBank: boolean = false;
        let foundBankID;
        deal.banksIDs.forEach((bank) => {
        if (bank.status === 'replied' || bank.status === 'rejected' || bank.status === 'accepted') { return true; }
        (bank.bankID as any).email.forEach(bankEmail => {
                if (mail.from.some(tmpMail => tmpMail.address === bankEmail)) {
                    foundBank = true;
                    foundBankID = (bank as any)._id;
                }
            });
        });

        if (foundBank) {
            // tslint:disable-next-line:object-literal-key-quotes
            const update = { '$set': { 'banksIDs.$.status': 'replied' }, lastNotify: new Date().toISOString(), notify: true };
            if (deal.status !== 'rejected' && deal.status !== 'completed') { (update as any).status = 'onhold'; }
            await this.dealAPI.updateDeal(update, { 'banksIDs._id': foundBankID });
            Logger.log(`Deal ${ (deal.companyID as any).name } moved to onhold`);
        }

    }

    async getCode(code: string) {
        const pos = code.indexOf('#DL#');
        const sliceCode = code.slice(pos);
        return sliceCode.search(/\s/g) === -1 ? sliceCode.slice(4) : sliceCode.slice(4, sliceCode.search(/\s/g));
    }

}
