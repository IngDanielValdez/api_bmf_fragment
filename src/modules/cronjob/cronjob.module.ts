import { Module } from '@nestjs/common';
import { ScheduleModule } from 'nest-schedule';
import { CronjobService } from './cronjob.service';
import { PostmanModule } from '../postman/postman.module';
import { FilesModule } from '../files/files.module';
import { MailListenerService } from './mail-listener.service';
import { DealsModule } from '../deals/deals.module';
import { CompaniesModule } from '../companies/companies.module';
import { UsersModule } from '../users/users.module';

@Module({
    imports: [ScheduleModule.register(), PostmanModule, FilesModule, DealsModule, CompaniesModule, UsersModule, PostmanModule],
    providers: [CronjobService, MailListenerService],
})
export class CronjobModule {}
