import { Injectable, Logger } from '@nestjs/common';
import { NestSchedule, Timeout, Cron } from 'nest-schedule';
import { CompaniesService } from '../companies/companies.service';
import * as backup from 'mongodb-backup';
// import * as restore from 'mongodb-restore';
import * as path from 'path';
import * as fs from 'fs';
import { FilesService } from '../files/files.service';
import * as moment from 'moment';
import { DealsService } from '../deals/deals.service';
import { UsersService } from '../users/users.service';
import { PostmanService } from '../postman/postman.service';
@Injectable()
export class CronjobService extends NestSchedule {

    constructor(private companyAPI: CompaniesService, private dealAPI: DealsService,
                private fileAPI: FilesService, private userAPI: UsersService,
                private postmanAPI: PostmanService) {
        super();
    }

    // @Cron('0 8 * * *', { waiting: true })
    async lastBankActivity() {
        const from = moment().subtract(1, 'days').startOf('day');
        const to = moment().subtract(1, 'days').endOf('day');

        const deals = await this.dealAPI.getDealsForReport({ lastSubmitted: { $gte: from, $lte: to } });
        if (deals.length > 0) {
            const report = [];
            for (const deal of deals) {
                report.push({ name: (deal.companyID as any).name, banks: [] });

                for (const bankID of deal.banksIDs) {
                    if (bankID.status === 'onhold') {
                        report[report.length - 1].banks.push((bankID.bankID as any).name);
                    }
                }
            }

            // prepare email
            let mail = '';
            for (const deal of report) {
                if (deal.banks.length > 0) {
                    mail += `${ deal.name }\nThe following banks have not responded:\n\n`;
                    mail += deal.banks.join('\n');
                }
                mail += `\n\n`;
            }

            const system = this.userAPI.SYSUSER._id.toString();
            const message = { to: 'richard@richardtaty.com', subject: `Bank response report`, body: mail, insertBy: system };
            await this.postmanAPI.createMail(message);
        }
    }

    // @Cron('0 1 * * *', { waiting: true })
    async disableInactiveUsers() {
        const min = moment().subtract(15, 'days').startOf('day');
        const users = await this.userAPI.getUsers({ options: { lastActivity: { $lte: min }, active: true } });
        let mail = 'The following accounts have been disabled:\nReason: 15 days of inactivity.\n\n';
        for (const user of users) {
            user.active = false;
            await user.save();
            mail += user.name + '\n';
        }

        const system = this.userAPI.SYSUSER._id.toString();
        const message = {
            to: 'richard@richardtaty.com', subject: `Users have been deactivated due to inactivity`,
            body: mail, insertBy: system };
        await this.postmanAPI.createMail(message);
        // await this.mailListener.banksListener();
    }

    // @Timeout(10000, { waiting: true })
    async dealListener() {
        // await this.mailListener.banksListener();
    }

    // @Timeout(3000, { waiting: true })
    // async activityReport() {
    //     const report = [];
    //     const users = await this.userAPI.getUsers({});
    //     for (const user of users) {
    //         const companies = await this.companyAPI.getCompanies({ limit: 1, options: { insertBy: user._id } });
    //         if (companies.length > 0) {
    //             report.push({ agent: user.name, lastActivity: companies[0].createdOn });
    //         } else {
    //             report.push({ agent: user.name, lastActivity: 'never' });
    //         }
    //     }
    //     // console.log(report);
    //     // await this.mailListener.banksListener();
    // }

    // @Timeout(1000, { waiting: true })
    // async bankReport() {
    //     const from = moment('2019-10-18', 'YYYY-MM-DD').startOf('day');
    //     const to = moment('2019-11-18', 'YYYY-MM-DD').endOf('day');
    //     const deals = await this.dealAPI.getDeals({ options: { lastSubmitted: { $gte: from, $lte: to }} });
    //     const banks = [];
    //     deals.forEach(deal => {
    //         deal.banksIDs.forEach(bankID => {
    //             const pos = banks.map(bank => bank._id).indexOf((bankID.bankID as any)._id);
    //             if (pos === -1) {
    //                 let rejected = 0;
    //                 let accepted = 0;
    //                 let onhold = 0;
    //                 let replied = 0;
    //                 if (bankID.status === 'rejected') { rejected = 1;
    //                 } else if (bankID.status === 'accepted') { accepted = 1;
    //                 } else if (bankID.status === 'replied') { replied = 1;
    //                 } else { onhold = 1; }
    //                 banks.push({
    //                     name: (bankID.bankID as any).name, _id: (bankID.bankID as any)._id, sended: 1, accepted, rejected, onhold, replied
    //                 });
    //             } else {
    //                 banks[pos].sended += 1;
    //                 if (bankID.status === 'rejected') { banks[pos].rejected += 1;
    //                 } else if (bankID.status === 'accepted') { banks[pos].accepted += 1;
    //                 } else if (bankID.status === 'replied') { banks[pos].replied += 1;
    //                 } else { banks[pos].onhold += 1; }
    //             }
    //         });
    //     });
    //     console.log(banks);
    //     console.log(deals.length);
    // }

    @Cron('0 0 * * *')
    async backupDB() {
        const name = new Date().getTime();
        const dir = path.resolve(__dirname, `../../../../backup`);
        await backup({ uri: process.env.URIDB, root: dir, tar: name + '.tar',
        collections: ['admreports', 'applications', 'banklists', 'banks',
        'companies', 'deals', 'featuredemployees', 'postmen', 'precodes', 'senders', 'task', 'users'],
        callback: async (err) => {
            if (err) { Logger.log('cant create backup'); return true; }
            const file = fs.createReadStream(`${ dir }/${ name }.tar`);
            await this.fileAPI.uploadBackup(file, name + '.tar', 'backups/');
            fs.unlinkSync(`${ dir }/${ name }.tar`);
            Logger.log('Backup saved');
        } });
    }

    // @Timeout(1000, { waiting: true })
    // async restoreBackupDB() {
    //     const dir = path.resolve(__dirname, `../../../../backup/bmfcrm`);
    //     await restore({
    //     uri: 'mongodb://bmfuser:Boumer%40ng12@ec2-3-231-210-240.compute-1.amazonaws.com:27017/bmfcrm', root: dir,
    //     callback: async (err) => {
    //         if (err) { Logger.log('cant restore backup'); return true; }
    //         Logger.log('Backup restore');
    //     } });
    // }

}
