import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Activity } from './interfaces/activity.interface';

@Injectable()
export class ActivitiesService {
    constructor(@InjectModel('Activity') private ActivityModel: Model<Activity>) {}

    async getActivities(query?): Promise<Activity[]> {
        const { skip, options } = query;
        return await this.ActivityModel.find(options)
        .sort({ createdOn: 'desc' })
        .limit(50)
        .populate('user', 'name role')
        .select('-headers -browser -body -token')
        .skip(Math.abs(Number(skip)) || 0);
    }

    async createActivity(activity: Activity): Promise<Activity> {
        const activityDB = new this.ActivityModel(activity);
        return await activityDB.save();
    }

    async updateActivity(activity, ID: string): Promise<Activity> {
        return await this.ActivityModel.findOneAndUpdate({ _id: ID}, activity, { new: true, runValidators: true, context: 'query' });
    }

    async getActivity(options: object): Promise<Activity> {
        return await this.ActivityModel.findOne(options)
        .populate('user', 'name role');
    }

    async countActivities(query): Promise<number> {
        const { options } = query;
        return await this.ActivityModel.countDocuments(options);
    }
}
