import { Schema } from 'mongoose';

export const activitySchema = new Schema({
    ip: {
        required: true,
        type: String,
        trim: true,
        maxlength: 46,
    },
    method: {
        type: String,
        required: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    token: {
        type: String,
        trim: true,
        maxlength: 800,
    },
    browser: {
        type: String,
        trim: true,
    },
    priority: {
        type: Number,
        required: true,
    },
    url: {
        type: String,
    },
    headers: {
        type: Object,
        required: true,
    },
    body: {
        type: Object,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    finalStatus: {
        type: Number,
    },
});
