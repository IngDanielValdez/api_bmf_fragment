import { Document } from 'mongoose';
import { User } from 'src/modules/users/interfaces/user.interface';

export interface Activity extends Document {
    ip: string;
    method: string;
    token: string;
    browser: string;
    priority: number;
    user: User;
    url: string;
    headers: string;
    body: string;
    createdOn: Date;
    finalStatus: string;
}
