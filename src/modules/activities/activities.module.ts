import { Module, Global } from '@nestjs/common';
import { ActivitiesService } from './activities.service';
import { MongooseModule } from '@nestjs/mongoose';
import { activitySchema } from './schemas/activities.schema';
import { ActivitiesController } from './activities.controller';
@Global()
@Module({
  providers: [ActivitiesService],
  imports: [MongooseModule.forFeature([{ name: 'Activity', schema: activitySchema }])],
  exports: [ActivitiesService],
  controllers: [ActivitiesController],
})
export class ActivitiesModule {}
