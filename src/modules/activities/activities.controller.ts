import { Controller, Get, UseFilters, UseGuards, Query, NotFoundException, Param } from '@nestjs/common';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { ActivitiesService } from './activities.service';
import { Activity } from './interfaces/activity.interface';
import { RangeQueryPipe } from '../../common/pipes/range-query.pipe';

@Controller('activities')
export class ActivitiesController {
    constructor(private activityAPI: ActivitiesService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN']))
    async getActivities(@Query(new RangeQueryPipe('createdOn')) query): Promise<{ activities: Activity[], total: number }> {
        const activities = await this.activityAPI.getActivities(query);
        const total = await this.activityAPI.countActivities(query);
        return { activities, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN']))
    async getActivity(@Param('id', new ValidateObjectIdPipe()) activityID: string): Promise<Activity> {
        const activity = await this.activityAPI.getActivity({ _id: activityID });
        if (!activity) { throw new NotFoundException('Activity not found', 'E001'); }
        return activity;
    }
}
