import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Company } from './interfaces/company.interface';
import { CreateCompanyDTO } from './DTO/create-company.dto';
import { UpdateCompanyDTO } from './DTO/update-company.dto';
import { Precode } from './interfaces/precode.interface';
import * as uniqid from 'uniqid';

@Injectable()
export class CompaniesService {
    constructor(@InjectModel('Companies') private CompanyModel: Model<Company>,
                @InjectModel('Precode') private PrecodeModel: Model<Precode>) {}

    async getCompanies(query?): Promise<Company[]> {
        const { limit, skip, options } = query;
        return await this.CompanyModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate('insertBy', 'name role')
        .sort({ createdOn: 'desc' });
    }

    async getDirectCompanies(query): Promise<Company[]> {
        return await this.CompanyModel.find(query)
        .select('name taxid members dba')
        .sort({ createdOn: 'desc' });
    }

    async createCompany(company: Company | CreateCompanyDTO): Promise<Company> {
        const companyDB = new this.CompanyModel(company);
        return await companyDB.save();
    }

    async getCompany(options: object): Promise<Company> {
        return await this.CompanyModel.findOne(options)
        .populate('insertBy', 'name role');
    }

    async countCompanies(query) {
        const { options } = query;
        return await this.CompanyModel.countDocuments(options);
    }

    async updateCompany(company: UpdateCompanyDTO | Company, ID: string) {
        return await this.CompanyModel.findOneAndUpdate({ _id: ID}, company, { new: true, runValidators: true, context: 'query' });
    }

    async deleteCompany(ID: string): Promise<Company> {
        return await this.CompanyModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }

    async deletePrecode(code: string): Promise<Precode> {
        return await this.CompanyModel.findOneAndDelete({ code });
    }

    async createPrecode(insertBy): Promise<Precode> {
        let code = uniqid.process();
        let exist = true;
        while (exist) {
            const company = await this.getCompany({ code });
            company ? code = uniqid.process() :  exist = false;
        }
        const precode = { code, insertBy };
        const precodeDB = new this.PrecodeModel(precode);
        return await precodeDB.save();
    }

    async getPrecode(code: string): Promise<Precode> {
        return await this.PrecodeModel.findOne({ code });
    }
}
