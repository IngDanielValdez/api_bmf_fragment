import { Document } from 'mongoose';
import { Member } from './member.interface';
import { Loan } from './loan.interface';
import { Statement } from './statement.interface';

export interface Company extends Document {
    code: string;
    name: string;
    dba: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phone: string[];
    email: string[];
    service: string;
    members: Member[];
    taxid: string;
    insertBy: string;
    active: boolean;
    createdOn: Date;
    isSended: boolean;
    cardprocessor: boolean;
    otheraccounts: boolean;
    loans: Loan[];
    tmpFolder: string[];
    bankStatements: Statement[];
    cardStatements: Statement[];
    additionalAccounts: Statement[];
    app: string;
    blacklist: boolean;
 }
