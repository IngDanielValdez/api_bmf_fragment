import { Document } from 'mongoose';

export interface Statement extends Document {
    name: string;
    quantity: number;
    period: string;
    createdOn: Date;
}
