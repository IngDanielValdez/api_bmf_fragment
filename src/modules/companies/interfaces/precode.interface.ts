import { Document } from 'mongoose';

export interface Precode extends Document {
    code: string;
    insertBy: string;
    createdOn: Date;
}
