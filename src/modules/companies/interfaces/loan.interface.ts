import { Document } from 'mongoose';

enum PaymentTypes { daily = 'daily', weekly = 'weekly', monthly = 'monthly' }

export interface Loan extends Document {
    amount: number;
    currentdebt: number;
    recurrentpayment: number;
    paymenttype: PaymentTypes;
}
