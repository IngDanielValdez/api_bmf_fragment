import { Module } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { CompaniesController } from './companies.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { companySchema } from './schemas/company.schema';
import { FilesModule } from '../files/files.module';
import { precodeSchema } from './schemas/precode.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Companies', schema: companySchema }]),
    MongooseModule.forFeature([{ name: 'Precode', schema: precodeSchema }]),
    FilesModule,
  ],
  controllers: [CompaniesController],
  providers: [CompaniesService],
  exports: [CompaniesService],
})
export class CompaniesModule {}
