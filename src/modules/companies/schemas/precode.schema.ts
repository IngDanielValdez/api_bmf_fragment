import { Schema } from 'mongoose';

export const precodeSchema = new Schema({
    code: {
        type: String,
        trim: true,
        required: true,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
