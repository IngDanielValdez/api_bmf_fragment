import { Schema } from 'mongoose';

export const loanSchema = new Schema({
    amount: {
        type: Number,
        min: 0,
        required: true,
    },
    currentdebt: {
        type: Number,
        min: 0,
        required: true,
    },
    recurrentpayment: {
        type: Number,
        min: 0,
        required: true,
    },
    paymenttype: {
        type: String,
        enum: {
            values: ['daily', 'weekly', 'monthly'],
            message: 'Invalid payment type',
        },
        default: 'daily',
    },
});
