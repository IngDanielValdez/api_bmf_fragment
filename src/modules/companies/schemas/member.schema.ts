import { Schema } from 'mongoose';

export const memberSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 60,
        minlength: 2,
        trim: true,
    },
    title: {
        type: String,
        required: [true, 'title is required'],
        maxlength: 60,
        minlength: 3,
        trim: true,
    },
    ownership: {
        type: Number,
        max: 100,
        min: 1,
        required: [true, 'owner % is required'],
    },
    address: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    city: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    state: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    zip: {
        type: String,
        trim: true,
        maxlength: 30,
    },
    ssn: {
        type: String,
        maxlength: 30,
        required: true,
    },
    birthdate: {
        type: Date,
        required: [true, 'birthdate is required'],
    },
    phone: [{
        type: String,
        trim: true,
        // maxlength: 15,
        required: true,
    }],
    email: [{
        type: String,
        required: [true, 'email is required'],
        maxlength: 60,
        trim: true,
        lowercase: true,
    }],
});
