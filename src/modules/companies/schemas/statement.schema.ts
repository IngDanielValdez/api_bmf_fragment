import { Schema } from 'mongoose';

export const statementSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    quantity: {
        type: Number,
        min: 0,
        default: 0,
    },
    period: {
        type: String,
        trim: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});
