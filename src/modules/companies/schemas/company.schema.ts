import { Schema } from 'mongoose';
import { memberSchema } from './member.schema';
import { loanSchema } from './loan.shcema';
import { statementSchema } from './statement.schema';

export const companySchema = new Schema({
    code: {
        type: String,
        required: true,
        trim: true,
        unique: true,
    },
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 60,
        minlength: 1,
        trim: true,
        unique: true,
    },
    dba: {
        type: String,
        maxlength: 60,
        trim: true,
    },
    address: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    city: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    state: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    zip: {
        type: String,
        trim: true,
        maxlength: 9,
    },
    phone: [{
        type: String,
        trim: true,
        // maxlength: 15,
        required: true,
    }],
    email: [{
        type: String,
        required: [true, 'email is required'],
        maxlength: 60,
        trim: true,
        lowercase: true,
    }],
    service: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    members: [memberSchema],
    taxid: {
        type: String,
        maxlength: 30,
        required: true,
        unique: true,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    isSended: {
        type: Boolean,
        default: false,
    },
    cardprocessor: {
        type: Boolean,
        default: false,
    },
    otheraccounts: {
        type: Boolean,
        default: false,
    },
    loans: [loanSchema],
    tmpFolder: {
        type: String,
    },
    bankStatements: [statementSchema],
    cardStatements: [statementSchema],
    additionalAccounts: [statementSchema],
    app: {
        type: String,
    },
    blacklist: {
        type: Boolean,
        default: false,
    },
 });
