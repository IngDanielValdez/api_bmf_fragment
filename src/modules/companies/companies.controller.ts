import { Controller, Get, UseFilters, UseGuards, Post, Body,
    Req, Query, Put, Param, Delete, NotFoundException, UseInterceptors, UploadedFile,
    Res, BadRequestException, InternalServerErrorException, ForbiddenException, ConflictException, Logger } from '@nestjs/common';
import { FileInterceptor  } from '@nestjs/platform-express';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateCompanyDTO } from './DTO/create-company.dto';
import { CompaniesService } from './companies.service';
import { User } from '../users/interfaces/user.interface';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { Company } from './interfaces/company.interface';
import { UpdateCompanyDTO } from './DTO/update-company.dto';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { ValidFiledExtensionsInterceptor } from '../../common/filters/valid-filed-extensions.filter';
import { FilesService } from '../files/files.service';
import { Precode } from './interfaces/precode.interface';
import { CompanyFileDTO } from './DTO/get-company-file.dto';
import { DeleteFileDTO } from './DTO/delete-file.dto';
import * as underscore from 'underscore';

@Controller('companies')
export class CompaniesController {
    constructor(private companyAPI: CompaniesService, private filesAPI: FilesService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getCompanies(@Query(new MatchQueryPipe(['name', 'dba', 'taxid', 'service', 'code'])) query, @Req() req):
    Promise<{ companies: Company[], total: number }> {
        const tokenUser = req.token.user as User;
        if (tokenUser.role === 'USER') { query = { options: { taxid: query.taxid } }; }
        const companies = await this.companyAPI.getCompanies(query);
        const total = await this.companyAPI.countCompanies(query);
        return { companies, total };
    }

    @Get('blacklist')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard)
    async getCompaniesBlackList():
    Promise<Company[]> {
        return await this.companyAPI.getDirectCompanies({ active: true, blacklist: true });
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getCompany(@Param('id', new ValidateObjectIdPipe()) companyID: string): Promise<Company> {
        const company = await this.companyAPI.getCompany({ _id: companyID, active: true });
        if (!company) { throw new NotFoundException('Company not found', '5001'); }
        return company;
    }

    @Get('precode/generate')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getPrecode(@Req() req): Promise<Precode> {
        const tokenUser = req.token.user as User;
        return await this.companyAPI.createPrecode(tokenUser._id);
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async createCompany(@Req() req, @Body(new PickPipe(['name', 'dba', 'address',
    'city', 'state', 'zip', 'phone', 'email', 'service', 'members', 'taxid', 'otheraccounts',
    'cardprocessor', 'loans', 'bankStatements', 'cardStatements', 'additionalAccounts', 'app', 'code'])) company: CreateCompanyDTO):
    Promise <Company> {
        const precode = await this.companyAPI.getPrecode(company.code);
        if (!precode) { throw new ForbiddenException('This code does not exist, or is already in use', '5002'); }
        await this.companyAPI.deletePrecode(precode.code);
        const tokenUser = req.token.user as User;
        Object.assign(company, { insertBy: tokenUser._id });
        return await this.companyAPI.createCompany(company);
    }

    @Put(':id')
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async updateCompany(@Param('id', new ValidateObjectIdPipe()) companyID, @Body(new PickPipe(['name', 'dba', 'address',
    'city', 'state', 'zip', 'phone', 'email', 'service', 'members', 'cardprocessor', 'loans', 'bankStatements', 'cardStatements',
    'otheraccounts', 'additionalAccounts', 'app', 'blacklist'])) company: UpdateCompanyDTO) {
        const period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
        const update = underscore.omit(company, ['bankStatements', 'cardStatements', 'additionalAccounts']);
        const companyDB = await this.companyAPI.getCompany({ _id: companyID });
        if (!companyDB) { throw new NotFoundException('Company not found', '5001'); }

        if (company.bankStatements) {
            update.bankStatements = company.bankStatements;
            companyDB.bankStatements.forEach((statement) => {
                if (statement.period !== period) {
                    update.bankStatements.push(statement);
                }
            });
        }

        if (company.cardStatements) {
            update.cardStatements = company.cardStatements;
            companyDB.cardStatements.forEach((statement) => {
                if (statement.period !== period) {
                    update.cardStatements.push(statement);
                }
            });
        }

        if (company.additionalAccounts) {
            update.additionalAccounts = company.additionalAccounts;
            companyDB.additionalAccounts.forEach((statement) => {
                if (statement.period !== period) {
                    update.additionalAccounts.push(statement);
                }
            });
        }
        return await this.companyAPI.updateCompany(update, companyID);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteCompany(@Param('id', new ValidateObjectIdPipe()) companyID): Promise<Company> {
        return await this.companyAPI.deleteCompany(companyID);
    }

    @Post('upload/:code')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 2500000 }}),
    new ValidFiledExtensionsInterceptor(['application/pdf']))
    async uploadFile(@UploadedFile('file') file: Express.Multer.File, @Param('code') code, @Res() res, @Query() query) {
        if (!file) { throw new BadRequestException('File not found in our server', '7001'); }
        let rute = code;
        if ('subfolder' in query) { rute += '/' + query.subfolder; }
        const exist = await this.filesAPI.fileExist(rute, file.originalname);
        if (exist) { throw new ConflictException('There is a file with this name, rename the file you are trying to upload and try again', '7002'); }
        const uploaded = await this.filesAPI.uploadFile(file, rute);
        if (!uploaded) { throw new InternalServerErrorException('Could not upload file', '7003'); }
        return res.json({ key: uploaded.Key });
    }

    @Get('file/download')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getFile(@Query() query: CompanyFileDTO, @Res() res) {
        const exist = await this.filesAPI.fileExist(query.path, query.file);
        if (!exist) { throw new NotFoundException('File not found', '7001'); }
        const file = await this.filesAPI.getFile(query.path, query.file);
        res.writeHead(200, {'Content-Type': file.ContentType });
        res.write( file.Body, 'binary');
        res.end(null, 'binary');
    }

    @Get('file/attachments/:code')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async listAll(@Param('code') code) {
        const files = await this.filesAPI.listFiles(code + '/attachments');
        return files.Contents;
    }

    @Delete('file/attachments/:code')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async deleteAttachment(@Param('code') code, @Query() query: DeleteFileDTO) {
        const exist = await this.filesAPI.fileExist(`${ code }/attachments`, query.name);
        if (!exist) { throw new NotFoundException('File not found', '7001'); }
        const deleted = await this.filesAPI.deleteFile(`${ code }/attachments`, query.name);
        return deleted;
    }
}
