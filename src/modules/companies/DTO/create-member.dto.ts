import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, Length, IsArray, Max, Min, IsNumber, IsDateString } from 'class-validator';

export class CreateMemberDTO {
    @IsNotEmpty()
    @MaxLength(60)
    @MinLength(2)
    @IsString()
    name: string;

    @IsNotEmpty()
    @MaxLength(60)
    @MinLength(3)
    @IsString()
    title: string;

    @IsNotEmpty()
    @Max(100)
    @Min(1)
    @IsNumber()
    ownership: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    address: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    city: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    state: string;

    @IsNotEmpty()
    @Length(5)
    @IsString()
    zip: string;

    @IsNotEmpty()
    @MaxLength(30)
    @MinLength(9)
    @IsString()
    ssn: string;

    @IsDateString()
    @IsNotEmpty()
    birthdate: string;

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(15, { each: true})
    @MinLength(10, { each: true })
    phone: string[];

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(60, { each: true})
    @IsEmail({}, { each: true })
    email: string[];
}
