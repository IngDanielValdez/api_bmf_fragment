import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteFileDTO {
    @IsNotEmpty()
    @IsString()
    name: string;
}
