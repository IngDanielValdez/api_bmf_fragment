import { IsNotEmpty, IsString } from 'class-validator';

export class CompanyFileDTO {
    @IsString()
    @IsNotEmpty()
    path: string;

    @IsString()
    @IsNotEmpty()
    file: string;
}
