import { IsNumber, Min, IsNotEmpty, IsString } from 'class-validator';

export class CreateStatementDTO {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNumber()
    @Min(0)
    @IsNotEmpty()
    quantity: number;

    @IsString()
    @IsNotEmpty()
    period: string;
}
