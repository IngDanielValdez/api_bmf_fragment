import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, Length, IsArray, IsBoolean, ValidateNested, IsOptional } from 'class-validator';
import { CreateMemberDTO } from './create-member.dto';
import { Type } from 'class-transformer';
import { CreateLoanDTO } from './create-loan.dto';
import { CreateStatementDTO } from './create-statement.dto';

export class UpdateCompanyDTO {
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(60)
    @MinLength(1)
    @IsString()
    name: string;

    @IsOptional()
    @MaxLength(60)
    @IsString()
    dba: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    address: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    city: string;

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    state: string;

    @IsOptional()
    @IsNotEmpty()
    @Length(5)
    @IsString()
    zip: string;

    @IsOptional()
    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(15, { each: true})
    @MinLength(10, { each: true })
    phone: string[];

    @IsOptional()
    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(60, { each: true})
    @IsEmail({}, { each: true })
    email: string[];

    @IsOptional()
    @MaxLength(150)
    @IsString()
    service: string;

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    members: CreateMemberDTO[];

    @IsOptional()
    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    taxid: string;

    @IsOptional()
    @IsBoolean()
    @IsNotEmpty()
    cardprocessor: boolean;

    @IsOptional()
    @IsBoolean()
    @IsNotEmpty()
    otheraccounts: boolean;

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateLoanDTO)
    loans: CreateLoanDTO[];

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    bankStatements: CreateStatementDTO[];

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    cardStatements: CreateStatementDTO[];

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    additionalAccounts: CreateStatementDTO[];

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    app: string;

    @IsOptional()
    @IsBoolean()
    blacklist: boolean;
}
