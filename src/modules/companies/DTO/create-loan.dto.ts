import { IsNumber, Min, IsNotEmpty, IsString, IsEnum, IsDefined } from 'class-validator';

export class CreateLoanDTO {
    @IsDefined()
    @IsNumber()
    @Min(0)
    @IsNotEmpty()
    amount: number;

    @IsNumber()
    @Min(0)
    @IsNotEmpty()
    currentdebt: number;

    @IsNumber()
    @Min(0)
    @IsNotEmpty()
    recurrentpayment: number;

    @IsString({ each: true })
    @IsEnum(['daily', 'weekly', 'monthly'])
    @IsNotEmpty()
    paymenttype: string;
}
