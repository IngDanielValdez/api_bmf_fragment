import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './interfaces/task.interface';
import { CreateTaskDTO } from './DTO/create-task.dto';

@Injectable()
export class CalendarService {
    constructor(@InjectModel('Task') private TaskModel: Model<Task>) {}

    async getTasks(query?): Promise<Task[]> {
        const { limit, skip, options } = query;
        return await this.TaskModel.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .populate('insertBy', 'name role')
        .populate('assignTo', 'name role')
        .populate({
            path: 'reference',
            select: 'companyID',
            populate: {
                path: 'companyID',
                model: 'Companies',
                select: 'name',
            },
        })
        .sort({ reminderDate: 'desc' });
    }

    async createTask(task: Task | CreateTaskDTO): Promise<Task> {
        const taskDB = new this.TaskModel(task);
        return await taskDB.save();
    }

    async getTask(options: object): Promise<Task> {
        return await this.TaskModel.findOne(options)
        .populate('insertBy', 'name role')
        .populate({
            path: 'reference',
            select: 'companyID',
            populate: {
                path: 'companyID',
                model: 'Companies',
                select: 'name',
            },
        })
        .populate('assignTo', 'name role');
    }

    async updateTask(task, ID: string) {
        return await this.TaskModel.findOneAndUpdate({ _id: ID}, task, { new: true, runValidators: true, context: 'query' })
        .populate('insertBy', 'name role')
        .populate({
            path: 'reference',
            select: 'companyID',
            populate: {
                path: 'companyID',
                model: 'Companies',
                select: 'name',
            },
        })
        .populate('assignTo', 'name role');
    }

    async deleteTask(ID: string): Promise<Task> {
        return await this.TaskModel.findOneAndUpdate({ _id: ID }, { active: false }, { new: true, runValidators: true, context: 'query' });
    }
}
