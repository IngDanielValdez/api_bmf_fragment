import { Document } from 'mongoose';

export interface Task extends Document {
    title: string;
    description: string;
    active: boolean;
    createdOn: string;
    reminderDate: string;
    insertBy: string;
    assignTo: string;
    reference: string;
    status: string;
}
