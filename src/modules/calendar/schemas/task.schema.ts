import { Schema } from 'mongoose';

export const TaskSchema = new Schema({
    title: {
        type: String,
        required: [true, 'title is required'],
        maxlength: 120,
        minlength: 3,
        trim: true,
    },
    description: {
        type: String,
        required: [true, 'description is required'],
        maxlength: 800,
        minlength: 3,
        trim: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    reminderDate: {
        type: Date,
        default: Date.now,
        required: true,
    },
    insertBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    assignTo: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    reference: {
        type: Schema.Types.ObjectId,
        ref: 'Deal',
        required: false,
    },
    status: {
        type: String,
        enum: ['completed', 'incomplete'],
        default: 'incomplete',
    },
});
