import { IsOptional, IsEnum, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class GetTasksDTO {
    @IsOptional()
    @IsEnum(['completed', 'incomplete'])
    status: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(24)
    @MinLength(24)
    assignTo: string;
}
