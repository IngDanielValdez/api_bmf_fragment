import { IsOptional, IsNotEmpty, IsString, MaxLength, MinLength, IsDateString } from 'class-validator';

export class CreateTaskDTO {
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(120)
    title: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(800)
    description: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(24)
    @MinLength(24)
    assignTo: string;

    @IsOptional()
    @IsString()
    @MaxLength(24)
    @MinLength(24)
    reference: string;

    @IsDateString()
    @IsNotEmpty()
    reminderDate: string;
}
