import { Module, Global } from '@nestjs/common';
import { CalendarController } from './calendar.controller';
import { CalendarService } from './calendar.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskSchema } from './schemas/task.schema';
import { UsersModule } from '../users/users.module';
import { DealsModule } from '../deals/deals.module';

@Global()
@Module({
  imports: [MongooseModule.forFeature([{ name: 'Task', schema: TaskSchema }]), UsersModule, DealsModule],
  controllers: [CalendarController],
  providers: [CalendarService],
  exports: [CalendarService],
})
export class CalendarModule {}
