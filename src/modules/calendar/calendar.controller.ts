import { Controller, Get, UseFilters, UseGuards, Query, Req, UnauthorizedException,
    NotFoundException, Param, Post, Body, Delete, Put, Logger } from '@nestjs/common';
import { CalendarService } from './calendar.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { CastErrorFilter } from '../../common/filters/cast-error.filter';
import { RoleGuard } from '../../common/guard/role.guard';
import { AuthGuard } from '../../common/guard/auth.guard';
import { MatchQueryPipe } from '../../common/pipes/math-query.pipe';
import { RangeQueryPipe } from '../../common/pipes/range-query.pipe';
import { Task } from './interfaces/task.interface';
import { UsersService } from '../users/users.service';
import { User } from '../users/interfaces/user.interface';
import { GetTasksDTO } from './DTO/get-tasks.dto';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateTaskDTO } from './DTO/create-task.dto';
import { DealsService } from '../deals/deals.service';
import { ExactQueryPipe } from '../../common/pipes/exact-query.pipe';

@Controller('calendar')
export class CalendarController {
    constructor(private calendarAPI: CalendarService, private userAPI: UsersService, private dealAPI: DealsService) {}

    @Get()
    @UseFilters(MongoErrorFilter, CastErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getTasks(@Req() req, @Query(new MatchQueryPipe(['status']), new ExactQueryPipe(['assignTo']),
    new RangeQueryPipe('reminderDate')) query: GetTasksDTO): Promise<Task[]> {
        const user = await this.userAPI.getUser({ _id: query.assignTo });
        if (!user) { throw new NotFoundException('User not found', '1001'); }
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== user._id.toString()) {
            throw new UnauthorizedException('insufficient permissions');
        }
        if ((tokenUser.role === 'ADMIN' || tokenUser.role === 'SUPERADMIN') && tokenUser._id === user._id.toString()) {
            if (query.hasOwnProperty('options') && (query as any).hasOwnProperty('assignTo')) {
                const assignTo = (query as any).options.assignTo;
                (query as any).options.assignTo = { $in: [assignTo, this.userAPI.SYSUSER._id.toString()] };
            }
        }
        return await this.calendarAPI.getTasks(query);
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getTask(@Param('id', new ValidateObjectIdPipe()) id, @Req() req): Promise<Task> {
        const task = await this.calendarAPI.getTask({ _id: id });
        if (!task) { throw new NotFoundException('Task not found', '4001'); }
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== (task.assignTo as any)._id.toString()) {
            throw new UnauthorizedException('insufficient permissions');
        }
        return task;
    }

    @Put(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async updateTask(@Req() req, @Param('id', new ValidateObjectIdPipe()) taskID): Promise<Task> {
        const task = await this.calendarAPI.getTask({ _id: taskID });
        if (!task) { throw new NotFoundException('Task not found', '4001'); }
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== (task.assignTo as any)._id.toString()) {
            throw new UnauthorizedException('insufficient permissions');
        }
        return await this.calendarAPI.updateTask({ status: 'completed' }, taskID);
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async createTask(@Req() req, @Body(new PickPipe(['title', 'description', 'assignTo', 'reference', 'reminderDate'])) task: CreateTaskDTO):
    Promise <Task> {
        const user = await this.userAPI.getUser({ _id: task.assignTo });
        if (!user) { throw new NotFoundException('User not found', '1001'); }

        if (task.hasOwnProperty('reference') && task.reference.length !== 0) {
            const deal = await this.dealAPI.getDeal({ _id: task.reference });
            if (!deal) { throw new NotFoundException('Deal not found', '6001'); }
        }

        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== task.assignTo) {
            throw new UnauthorizedException('insufficient permissions');
        }

        Object.assign(task, { insertBy: tokenUser._id });
        return await this.calendarAPI.createTask(task);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async deleteTask(@Req() req, @Param('id', new ValidateObjectIdPipe()) taskID): Promise<Task> {
        const task = await this.calendarAPI.getTask({ _id: taskID });
        if (!task) { throw new NotFoundException('Task not found', '4001'); }
        const tokenUser = req.token.user as User;
        if (tokenUser.role !== 'ADMIN' && tokenUser.role !== 'SUPERADMIN' && tokenUser._id !== (task.assignTo as any)._id.toString()) {
            throw new UnauthorizedException('insufficient permissions');
        }
        return await this.calendarAPI.deleteTask(taskID);
    }

}
