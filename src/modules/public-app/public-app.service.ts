import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Application } from './interfaces/application.interface';
import { CreateApplicationDTO } from './DTO/application.dto';

@Injectable()
export class PublicAppService {
    constructor(@InjectModel('Application') private applicationAPI: Model<Application>) {}

    async createApplication(application: Application | CreateApplicationDTO): Promise<Application> {
        const appDB = new this.applicationAPI(application);
        return await appDB.save();
    }

    async getApplications(query?): Promise<Application[]> {
        const { limit, skip, options } = query;
        return await this.applicationAPI.find(options)
        .limit(Number(limit) > 100 ? 100 : Number(limit))
        .skip(Math.abs(Number(skip)) || 0)
        .sort({ createdOn: 'desc' });
    }

    async getApplication(options: object): Promise<Application> {
        return await this.applicationAPI.findOne(options);
    }

    async countApplications(query) {
        const { options } = query;
        return await this.applicationAPI.countDocuments(options);
    }
}
