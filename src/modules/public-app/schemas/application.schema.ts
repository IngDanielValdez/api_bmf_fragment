import { Schema } from 'mongoose';
import { memberSchema } from '../../companies/schemas/member.schema';
import { statementSchema } from '../../companies/schemas/statement.schema';
import { loanSchema } from '../../companies/schemas/loan.shcema';

export const applicationSchema = new Schema({
    code: {
        type: String,
        required: true,
        trim: true,
    },
    ipRequest: {
        type: String,
        trim: true,
        required: true,
    },
    lookingFor: {
      type: Number,
      min: 0,
    },
    name: {
        type: String,
        required: [true, 'name is required'],
        maxlength: 60,
        minlength: 1,
        trim: true,
    },
    dba: {
        type: String,
        maxlength: 60,
        trim: true,
    },
    address: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    city: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    state: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    zip: {
        type: String,
        trim: true,
        maxlength: 9,
    },
    phone: [{
        type: String,
        trim: true,
        maxlength: 20,
        required: true,
    }],
    email: [{
        type: String,
        required: [true, 'email is required'],
        maxlength: 60,
        trim: true,
        lowercase: true,
    }],
    service: {
        type: String,
        trim: true,
        maxlength: 150,
    },
    members: [memberSchema],
    taxid: {
        type: String,
        maxlength: 30,
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
    cardprocessor: {
        type: Boolean,
        default: false,
    },
    otheraccounts: {
        type: Boolean,
        default: false,
    },
    loans: [loanSchema],
    bankStatements: [statementSchema],
    cardStatements: [statementSchema],
    additionalAccounts: [statementSchema],
 });
