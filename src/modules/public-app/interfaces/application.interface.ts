import { Document } from 'mongoose';
import { Member } from '../../companies/interfaces/member.interface';
import { Loan } from '../../companies/interfaces/loan.interface';
import { Statement } from '../../companies/interfaces/statement.interface';

export interface Application extends Document {
    lookingFor: number;
    ipRequest: string;
    code: string;
    name: string;
    dba: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phone: string[];
    email: string[];
    service: string;
    members: Member[];
    taxid: string;
    createdOn: Date;
    cardprocessor: boolean;
    otheraccounts: boolean;
    loans: Loan[];
    bankStatements: Statement[];
    cardStatements: Statement[];
    additionalAccounts: Statement[];
 }
