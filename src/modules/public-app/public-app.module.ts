import { Module } from '@nestjs/common';
import { PublicAppController } from './public-app.controller';
import { PublicAppService } from './public-app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { applicationSchema } from './schemas/application.schema';
import { FilesModule } from '../files/files.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Application', schema: applicationSchema }]),
    FilesModule,
  ],
  controllers: [PublicAppController],
  providers: [PublicAppService],
})
export class PublicAppModule {}
