import { Controller, Get, Req, Res, Post, UseFilters, UseInterceptors,
    UploadedFile, Param, BadRequestException, Query, InternalServerErrorException, Body, UseGuards, NotFoundException } from '@nestjs/common';
import { PublicAppService } from './public-app.service';
import { MongoErrorFilter } from '../../common/filters/mongo-error.filter';
import { ValidFiledExtensionsInterceptor } from '../../common/filters/valid-filed-extensions.filter';
import { FileInterceptor } from '@nestjs/platform-express';
import { FilesService } from '../files/files.service';
import { PickPipe } from '../../common/pipes/pick.pipe';
import { CreateApplicationDTO } from './DTO/application.dto';
import { AuthGuard } from '../../common/guard/auth.guard';
import { RoleGuard } from '../../common/guard/role.guard';
import { Application } from './interfaces/application.interface';
import { ValidateObjectIdPipe } from '../../common/pipes/validate-object-id.pipe';

@Controller('application')
export class PublicAppController {
    constructor(private applicationAPI: PublicAppService, private filesAPI: FilesService) {}

    @Get()
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getApplications(@Req() req, @Query() query): Promise<{ applications: Application[], total: number }> {
        const applications = await this.applicationAPI.getApplications(query);
        const total = await this.applicationAPI.countApplications(query);
        return { applications, total };
    }

    @Get(':id')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT']))
    async getApplication(@Param('id', new ValidateObjectIdPipe()) applicationID: string): Promise<Application> {
        const application = await this.applicationAPI.getApplication({ _id: applicationID });
        if (!application) { throw new NotFoundException('Application not found', '5001'); }
        return application;
    }

    @Post()
    @UseFilters(MongoErrorFilter)
    async registerApp(@Req() req, @Body(new PickPipe(['name', 'lookinFor', 'dba', 'address',
    'city', 'state', 'zip', 'phone', 'email', 'service', 'members', 'taxid', 'otheraccounts',
    'cardprocessor', 'loans', 'bankStatements', 'cardStatements', 'additionalAccounts', 'code'])) application: CreateApplicationDTO) {
        (application as any).ipRequest = req.header('x-forwarded-for') || req.connection.remoteAddress;
        return await this.applicationAPI.createApplication(application);
    }

    @Post('upload/:code')
    @UseInterceptors(FileInterceptor('file', { limits: { fileSize: 5000000 }}),
    new ValidFiledExtensionsInterceptor(['application/pdf']))
    async uploadFile(@UploadedFile('file') file: Express.Multer.File, @Param('code') code, @Res() res, @Query() query) {
        const period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
        const validFolders = ['additionalAccounts', 'cardprocessor', 'bankstatements'];
        if (!file) { throw new BadRequestException('Missing File', '7004'); }
        if (validFolders.indexOf(query.subfolder) === -1) { throw new BadRequestException('invalid statement type'); }
        const rute = `${ code }/${ period }/${ query.subfolder }/`;
        const uploaded = await this.filesAPI.uploadPublic(file, rute);
        if (!uploaded) { throw new InternalServerErrorException('Could not upload file', '7003'); }
        return res.json({ key: uploaded.Key });
    }

    @Get('file/download')
    @UseGuards(AuthGuard, new RoleGuard(['SUPERADMIN', 'MANAGER', 'ADMIN', 'SUPPORT', 'USER']))
    async getFile(@Query() query, @Res() res) {
        const exist = await this.filesAPI.fileExist(query.path, query.file, 'bmf-public');
        if (!exist) { throw new NotFoundException('File not found', '7001'); }
        const file = await this.filesAPI.getFile(query.path, query.file, 'bmf-public');
        res.writeHead(200, {'Content-Type': file.ContentType });
        res.write( file.Body, 'binary');
        res.end(null, 'binary');
    }

}
