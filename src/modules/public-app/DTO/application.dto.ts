import { IsNotEmpty, IsString, MaxLength, IsEmail, MinLength, Length, IsArray, IsBoolean,
    ValidateNested, IsOptional, IsEmpty, Min, Max, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateStatementDTO } from '../../companies/DTO/create-statement.dto';
import { CreateLoanDTO } from '../../companies/DTO/create-loan.dto';
import { CreateMemberDTO } from '../../companies/DTO/create-member.dto';

export class CreateApplicationDTO {
    @IsNotEmpty()
    @MaxLength(60)
    @MinLength(1)
    @IsString()
    name: string;

    @IsNotEmpty()
    @Min(0)
    @Max(250000)
    @IsNumber()
    lookingFor: number;

    @MaxLength(60)
    @IsString()
    dba: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    address: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    city: string;

    @IsNotEmpty()
    @MaxLength(150)
    @IsString()
    state: string;

    @IsNotEmpty()
    @Length(5)
    @IsString()
    zip: string;

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(15, { each: true})
    @MinLength(10, { each: true })
    phone: string[];

    @IsArray()
    @IsNotEmpty({ each: true })
    @IsString({ each: true })
    @MaxLength(60, { each: true})
    @IsEmail({}, { each: true })
    email: string[];

    @MaxLength(150)
    @IsString()
    service: string;

    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    members: CreateMemberDTO[];

    @IsNotEmpty()
    @MaxLength(30)
    @IsString()
    taxid: string;

    @IsBoolean()
    @IsNotEmpty()
    cardprocessor: boolean;

    @IsBoolean()
    @IsNotEmpty()
    otheraccounts: boolean;

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateLoanDTO)
    loans: CreateLoanDTO[];

    @IsArray()
    @ValidateNested({ each: true })
    @IsNotEmpty({ each: true })
    bankStatements: CreateStatementDTO[];

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    cardStatements: CreateStatementDTO[];

    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    additionalAccounts: CreateStatementDTO[];

    @IsString()
    @IsNotEmpty()
    code: string;
}
