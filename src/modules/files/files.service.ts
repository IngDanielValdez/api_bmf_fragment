import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { S3 } from 'aws-sdk';
import { PutObjectRequest } from 'aws-sdk/clients/s3';
import { ReadStream } from 'fs';

@Injectable()
export class FilesService {
    private S3: AWS.S3;

    constructor() {
        AWS.config.update({
            secretAccessKey: process.env.AWS_SC_KEY,
            accessKeyId: process.env.AWS_KEY,
            region: process.env.AWS_REGION,
        });
        this.S3 = new AWS.S3();
    }

    async fileExist(folder, file, bucket = process.env.AWS_BUCKET): Promise<boolean> {
        folder = folder.charAt(folder.length - 1 ) === '/' ?  folder.substr(0, folder.length - 1) : folder;
        const fileList = await this.listFiles(folder, bucket);
        if (fileList.KeyCount > 0) {
            let exist = false;
            fileList.Contents.forEach(fileS3 => fileS3.Key === `${folder}/${file}` ? exist = true : '');
            return exist;
        } else {
            return false;
        }
    }

    async listFiles(folder, bucket = process.env.AWS_BUCKET): Promise<S3.ListObjectsV2Output> {
        const params: S3.ListObjectsV2Request = {
            Bucket: bucket,
            Prefix: folder,
        };
        return await this.S3.listObjectsV2(params).promise();
    }

    async deleteFile(folder, file) {
        const params: S3.DeleteObjectRequest = {
            Bucket: process.env.AWS_BUCKET,
            Key: `${ folder }/${ file }`,
        };
        return await this.S3.deleteObject(params).promise();
    }

    async getFile(folder, file, bucket = process.env.AWS_BUCKET) {
        const params: S3.DeleteObjectRequest = {
            Bucket: bucket,
            Key: `${ folder }/${ file }`,
        };
        return await this.S3.getObject(params).promise();
    }

    async uploadFile(file: Express.Multer.File, folder: string): Promise<S3.ManagedUpload.SendData> {
        if (!file) { return null; }
        const params: PutObjectRequest  = {
            Bucket: process.env.AWS_BUCKET,
            Key: folder + file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
        };
        return await this.S3.upload(params).promise();
    }

    async uploadPublic(file: Express.Multer.File, folder: string): Promise<S3.ManagedUpload.SendData> {
        if (!file) { return null; }
        const params: PutObjectRequest  = {
            Bucket: 'bmf-public',
            Key: folder + file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
        };
        return await this.S3.upload(params).promise();
    }

    async uploadBackup(buffer: ReadStream, name, folder: string): Promise<S3.ManagedUpload.SendData> {
        const params: PutObjectRequest  = {
            Bucket: process.env.AWS_BUCKET,
            Key: folder + name,
            Body: buffer,
            ContentType: 'application/x-tar',
        };
        return await this.S3.upload(params).promise();
    }
}
