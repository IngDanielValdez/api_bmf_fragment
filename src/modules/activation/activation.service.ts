import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Activation } from './interfaces/activation.interface';
import { User } from '../users/interfaces/user.interface';
import * as moment from 'moment';
import * as nodemailer from 'nodemailer';

@Injectable()
export class ActivationService {
    constructor(@InjectModel('Activation') private ActivationModel: Model<Activation>) {}

    async getActivation(options) {
        return await this.ActivationModel.findOne(options);
    }

    async deleteActivation(options) {
        return await this.ActivationModel.deleteOne(options);
    }

    async createActivation(user: User) {
        const hash = Math.floor(100000 + Math.random() * 900000);
        const expiresOn = moment(new Date()).add(30, 'minutes').toISOString();
        const activation = await new this.ActivationModel({ user: user._id, hash, expiresOn });
        return await activation.save();
    }

    async resendActivation(user: User) {
        const deleteAny = await this.deleteActivation({ user: user._id });
        const newActivation = await this.createActivation(user);
        return await this.sendActivation(user, newActivation);
    }

    async sendActivation(user: User, activation: Activation) {
        const transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: 25,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_AUTH,
            },
        });

        return await transporter.sendMail({
            from: process.env.MAIL_AUTH_SENDER,
            to: user.email,
            subject: 'Activation code',
            text: `${ activation.hash }`,
        });
    }
}
