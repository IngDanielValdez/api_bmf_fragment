import { Module } from '@nestjs/common';
import { ActivationController } from './activation.controller';
import { ActivationService } from './activation.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ActivationSchema } from './schemas/activation.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Activation', schema: ActivationSchema }])],
  controllers: [ActivationController],
  providers: [ActivationService],
  exports: [ActivationService],
})
export class ActivationModule {}
