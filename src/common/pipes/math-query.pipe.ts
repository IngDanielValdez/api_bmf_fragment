import { Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class MatchQueryPipe implements PipeTransform {
  constructor(private keys: string[]) {}
  transform(value: any) {
    value.options = { active: !value.active ? true : value.active === 'true' ? true : false };
    this.keys.forEach(key => {
      if (value[key]) {
        value.options[key] = value[key] === ('true' || 'false') ? value[key] : new RegExp(value[key].replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'gi');
      }
    });
    return value;
  }
}
