import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ExactQueryPipe implements PipeTransform {
  constructor(private keys: string[]) {}
  transform(value: any) {
    if (!value.hasOwnProperty('options')) { value.options = {}; }
    this.keys.forEach(key => {
      if (value[key]) {
        value.options[key] = value[key];
      }
    });
    return value;
  }
}
