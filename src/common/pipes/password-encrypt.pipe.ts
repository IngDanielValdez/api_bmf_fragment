import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class PasswordEncryptPipe implements PipeTransform {
  constructor(private validatePassword = true) {}
  transform(value: any) {
    const strongValidation = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})');

    if (!value.password) { throw new BadRequestException('Missing password'); }
    if (this.validatePassword) {
      if (!strongValidation.test(value.password)) { throw new BadRequestException('The password is very weak', '1003'); }
    }
    value.password = bcrypt.hashSync(value.password, 10);
    return value;
  }
}
