import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class RangeQueryPipe implements PipeTransform {
  constructor(private input: string) {}
  key;
  transform(value: any, metadata: ArgumentMetadata) {
  if (!value.hasOwnProperty('options')) {
    value.options = {};
  }

  if (value.options.hasOwnProperty('status') && value.options.status.test('completed')) {
    this.key  = 'lastClosingDate';
  } else {
    this.key = this.input;
  }
  if (value.from || value.to) {
      if (value.options) {
        Object.assign(value.options, { [this.key]: {} });
        if (value.hasOwnProperty('from') && value.from.length > 0) {
          value.options[this.key].$gte  = moment(value.from, 'YYYY-MM-DD').startOf('day');
        }
        if (value.hasOwnProperty('to') && value.to.length > 0) { value.options[this.key].$lte = moment(value.to, 'YYYY-MM-DD').endOf('day'); }
      }
  }
  return value;
  }
}
