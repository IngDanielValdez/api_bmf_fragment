import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Logger } from '@nestjs/common';
import { ActivitiesService } from '../modules/activities/activities.service';
import { Request } from 'express';
import { tap } from 'rxjs/operators';
import { AuthService } from '../modules/auth/auth.service';
import * as mongoose from 'mongoose';
import { UsersService } from '../modules/users/users.service';
@Injectable()
export class ActivityInterceptor implements NestInterceptor {
  constructor(private activitiAPI: ActivitiesService, private authAPI: AuthService, private userAPI: UsersService) {}
  async intercept(context: ExecutionContext, next: CallHandler) {
    const activity: any = {};
    const req: Request = context.switchToHttp().getRequest();
    const HandlerName = context.getHandler().name;
    const ClassName = context.getClass().name;
    if ((HandlerName === 'getFile' && ClassName === 'UsersController') || ClassName === 'ActivitiesController' ||
         (ClassName === 'FeaturedEmployeeController' && HandlerName === 'getFile')) {
      return next.handle();
    } else {
    // Get the IP and Set the Method
    Object.assign(activity, { ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress, method: req.method });

    // Set Token and browser
    const token = req.get('Authorization');
    const browser = req.headers['user-agent'];
    Object.assign(activity, { token, browser, priority: this.getPriority(req.method) });

    // Set Body, Headers and URL
    Object.assign(activity, { body: req.body, headers: req.headers, url: req.originalUrl });

    try {
      const decoded = this.authAPI.verifyToken(token);
      const _id = (decoded as any).user._id;
      if (mongoose.Types.ObjectId.isValid(_id)) {
        Object.assign(activity, { user: _id });
        const userDB = await this.userAPI.getUser({ _id });
        if (userDB) {
          userDB.lastActivity = new Date();
          userDB.save();
        }
      }

    // tslint:disable-next-line:no-empty
    } catch (e) { }

    const actDB = await this.activitiAPI.createActivity(activity);
    (req as any).control = actDB._id.toString();

    return next.handle().pipe(tap(async () => {
      const res = context.switchToHttp().getResponse();
      const actDBAfter = await this.activitiAPI.getActivity({ _id: (req as any).control});
      if (actDBAfter) {
        actDBAfter.finalStatus = res.statusCode;
        await actDBAfter.save();
      }
    }));
    }
  }

  getPriority(type) {
    return type === 'GET' || type === 'POST' ? 3 : type === 'PUT' ? 2 : 1;
  }
}
