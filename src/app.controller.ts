import { Controller, Get, Res, UseInterceptors } from '@nestjs/common';
import { AppService } from './app.service';
import { ActivityInterceptor } from './common/activity.interceptor';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @UseInterceptors(ActivityInterceptor)
  getHello(@Res() res) {
    return res.json({ ok: true });
  }
}
