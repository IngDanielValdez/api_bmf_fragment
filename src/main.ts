import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { JwtErrorFilter } from './common/filters/jwt-error.filter';
import * as compression from 'compression';
import * as fs from 'fs';
import * as helmet from 'helmet';

let httpsOptions = {};
if (process.env.NODE_ENV === 'production') {
  httpsOptions = {
    key: fs.readFileSync('../keys/privkey.pem'),
    cert: fs.readFileSync('../keys/cert.pem'),
  };
}
async function bootstrap() {
  const app = process.env.NODE_ENV === 'production' ? await NestFactory.create(AppModule, { httpsOptions }) :
  await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors();
  app.useGlobalFilters(new JwtErrorFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.use(compression());
  app.listen(process.env.PORT);
}
bootstrap();
