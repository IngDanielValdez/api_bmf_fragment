import { Injectable, UseInterceptors } from '@nestjs/common';
import { ActivityInterceptor } from './common/activity.interceptor';

@Injectable()
export class AppService {

  getHello(): string {
    return 'CRM IS RUNNING';
  }
}
