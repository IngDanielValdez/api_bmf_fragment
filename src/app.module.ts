import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { ActivationModule } from './modules/activation/activation.module';
import { BanksModule } from './modules/banks/banks.module';
import { CompaniesModule } from './modules/companies/companies.module';
import { FilesModule } from './modules/files/files.module';
import { DealsModule } from './modules/deals/deals.module';
import { PostmanModule } from './modules/postman/postman.module';
import { FeaturedEmployeeModule } from './modules/featured-employee/featured-employee.module';
import { CalendarModule } from './modules/calendar/calendar.module';
import { CronjobModule } from './modules/cronjob/cronjob.module';
import { StatsModule } from './modules/stats/stats.module';
import { BankListModule } from './modules/bank-list/bank-list.module';
import { ReportsModule } from './modules/reports/reports.module';
import { PublicAppModule } from './modules/public-app/public-app.module';
import { ActivitiesModule } from './modules/activities/activities.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ActivityInterceptor } from './common/activity.interceptor';
import { ContactListModule } from './modules/contact-list/contact-list.module';

const mongoConfig = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
};
@Module({
  imports: [
    MongooseModule.forRoot(process.env.URIDB, mongoConfig),
    UsersModule,
    AuthModule,
    ActivationModule,
    BanksModule,
    CompaniesModule,
    FilesModule,
    DealsModule,
    PostmanModule,
    FeaturedEmployeeModule,
    CalendarModule,
    CronjobModule,
    StatsModule,
    BankListModule,
    ReportsModule,
    PublicAppModule,
    ActivitiesModule,
    ContactListModule,
  ],
  controllers: [AppController],
  providers: [AppService, { provide: APP_INTERCEPTOR, useClass: ActivityInterceptor}],
})
export class AppModule {}
